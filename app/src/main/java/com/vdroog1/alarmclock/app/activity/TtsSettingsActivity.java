package com.vdroog1.alarmclock.app.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import com.vdroog1.alarmclock.app.R;
import com.vdroog1.alarmclock.app.fragment.TTSSettingsFragment;

/**
 * Created by KaterinaG on 11.11.2014.
 * Activity container for TTS settings fragment
 */
public class TtsSettingsActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_activity);

        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = fragmentManager.findFragmentByTag(TTSSettingsFragment.TAG);
        if (fragment == null) {
            fragment = new TTSSettingsFragment();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.add(R.id.fragment_container, fragment, TTSSettingsFragment.TAG);
            fragmentTransaction.commit();
        }
    }
}
