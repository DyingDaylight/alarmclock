package com.vdroog1.alarmclock.app.activity;

import android.content.*;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import com.vdroog1.alarmclock.app.R;
import com.vdroog1.alarmclock.app.Util;
import com.vdroog1.alarmclock.app.fragment.AlarmsListFragment;
import com.vdroog1.alarmclock.app.fragment.SettingsFragment;
import com.vdroog1.alarmclock.app.interfaces.AlarmServiceListener;
import com.vdroog1.alarmclock.app.interfaces.OnAlarmSelected;
import com.vdroog1.alarmclock.app.interfaces.SettingsCompleteListener;
import com.vdroog1.alarmclock.app.service.AlarmService;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends ActionBarActivity implements OnAlarmSelected,
        SettingsCompleteListener
{

    public static final String IS_FIRST_RUN = "isFirstRun";
    public static final int UPDATE_REQUEST_CODE = 102;

    List<AlarmServiceListener> fragmentList = new ArrayList<AlarmServiceListener>();

    View imageView;
    View detailFragmentContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.pane_activity);

        imageView = findViewById(R.id.image);
        detailFragmentContainer = findViewById(R.id.detailFragment);

        if (savedInstanceState == null && imageView != null) {
            imageView.setVisibility(View.VISIBLE);
        }

        if (detailFragmentContainer != null && savedInstanceState != null) {
            int visibility = savedInstanceState.getInt("isVisible");
            if (visibility == View.VISIBLE) {
                detailFragmentContainer.setVisibility(View.VISIBLE);
                imageView.setVisibility(View.GONE);
            } else {
                detailFragmentContainer.setVisibility(View.INVISIBLE);
                imageView.setVisibility(View.VISIBLE);
            }
        }

        FragmentManager fragmentManager = getSupportFragmentManager();
        AlarmsListFragment fragment = (AlarmsListFragment) fragmentManager.findFragmentByTag(AlarmsListFragment.TAG);
        if (fragment == null) {
            fragment = new AlarmsListFragment();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.add(R.id.listFragment, fragment, AlarmsListFragment.TAG);
            fragmentTransaction.commit();
        }
        if (!fragmentList.contains(fragment)){
            fragmentList.add(fragment);
        }

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        if (prefs.getBoolean(IS_FIRST_RUN, true)) {
            startSettingsActivity();
            prefs.edit().putBoolean(IS_FIRST_RUN, false).apply();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.options, menu);
        return true;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (detailFragmentContainer != null)
            outState.putInt("isVisible", detailFragmentContainer.getVisibility());
    }

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter(AlarmService.UPDATE);
        registerReceiver(mMessageReceiver, filter);
        if (detailFragmentContainer != null && imageView != null) {
            if (detailFragmentContainer.getVisibility() == View.VISIBLE)
                imageView.setVisibility(View.GONE);
            else
                imageView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(mMessageReceiver );
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == UPDATE_REQUEST_CODE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            handleAction(extras);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                startSettingsActivity();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onAlarmSelected(int id) {
        if (detailFragmentContainer == null) {
            Intent intent = new Intent(this, SettingsActivity.class);
            intent.putExtra(Util.ALARM_ID, id);
            startActivityForResult(intent, UPDATE_REQUEST_CODE);
        } else {
            openAlarm(id);
        }
    }

    @Override
    public void onSettingsDone(int action, int id) {
        if (detailFragmentContainer == null)
            return;
        if (action == Util.ACTION_CANCEL || action == Util.ACTION_DELETE) {
            detailFragmentContainer.setVisibility(View.INVISIBLE);
            if (imageView != null) imageView.setVisibility(View.VISIBLE);
        }
    }

    private void openAlarm(int id) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        SettingsFragment fragment =  (SettingsFragment) fragmentManager.findFragmentByTag(SettingsFragment.TAG);
        if (fragment == null) {
            fragment = new SettingsFragment();
            Bundle arguments = new Bundle();
            arguments.putInt(Util.ALARM_ID, id);
            fragment.setArguments(arguments);

            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.add(R.id.detailFragment, fragment, SettingsFragment.TAG);
            fragmentTransaction.commit();
        } else {
            fragment.showAlarm(id);
        }
        if (!fragmentList.contains(fragment)){
            fragmentList.add(fragment);
        }
        if (detailFragmentContainer != null) detailFragmentContainer.setVisibility(View.VISIBLE);
        if (imageView != null) imageView.setVisibility(View.GONE);
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle extras = intent.getExtras();
            handleAction(extras);
        }
    };

    private void handleAction(Bundle extras) {
        if (extras != null) {
            int action = extras.getInt(Util.ACTION);
            int id = extras.getInt(Util.ALARM_ID);
            if (action == Util.ACTION_DELETE) {
                for (AlarmServiceListener listener : fragmentList) {
                    listener.onAlarmDeleted(id);
                }
            } if (action == Util.ACTION_SAVE) {
                for (AlarmServiceListener listener : fragmentList) {
                    listener.onAlarmSaved(id);
                }
            } if (action == Util.ACTION_UPDATE) {
                boolean isEnabled = extras.getBoolean(Util.ENABLED);
                for (AlarmServiceListener listener : fragmentList) {
                    listener.onAlarmEnabled(isEnabled, id);
                }
            }
        }
    }

    private void startSettingsActivity() {
        Intent intent = new Intent(this, TtsSettingsActivity.class);
        startActivity(intent);
    }
}
