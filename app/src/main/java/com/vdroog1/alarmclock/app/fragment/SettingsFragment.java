package com.vdroog1.alarmclock.app.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import com.vdroog1.alarmclock.app.DB.DataBaseHelper;
import com.vdroog1.alarmclock.app.DB.HelperFactory;
import com.vdroog1.alarmclock.app.R;
import com.vdroog1.alarmclock.app.Util;
import com.vdroog1.alarmclock.app.dialog.*;
import com.vdroog1.alarmclock.app.interfaces.AlarmServiceListener;
import com.vdroog1.alarmclock.app.interfaces.SettingsCompleteListener;
import com.vdroog1.alarmclock.app.interfaces.SettingsDialogsListener;
import com.vdroog1.alarmclock.app.model.Alarm;
import com.vdroog1.alarmclock.app.model.Day;
import com.vdroog1.alarmclock.app.service.AlarmService;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by KaterinaG on 29.11.2014.
 * Fragment with alarm settings
 */
public class SettingsFragment extends Fragment implements View.OnClickListener,
        CompoundButton.OnCheckedChangeListener,
        AlarmServiceListener,
        SettingsDialogsListener,
        LoaderManager.LoaderCallbacks<Cursor>{

    public static final String TAG = SettingsFragment.class.getSimpleName();

    private Alarm alarm;
    private ArrayList<Day> days = new ArrayList<Day>();

    private String[] alarmsUri;
    private String[] alarmsName;

    private boolean wasDisabled = false;

    private DataBaseHelper dbHelper;

    private SettingsCompleteListener listener;

    private ProgressDialog progressDialog;

    private int checkedMusicItem = 0;

    private CheckBox cbAlarmOn;
    private TextView tvTime;
    private TextView tvDays;
    private CheckBox cbRepeatOn;
    private View bInterval;
    private TextView tvInterval;
    private View bRepeatNum;
    private TextView tvRepeatNum;
    private TextView tvRingtone;
    private SeekBar sbVolume;
    private CheckBox cbVibrateOn;
    private EditText tvNote;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRetainInstance(true);

        dbHelper = HelperFactory.getHelper();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.alarm_settings, container, false);

        View alarmOn = mView.findViewById(R.id.enable_alarm);
        View bTime = mView.findViewById(R.id.b_time);
        View bDays = mView.findViewById(R.id.b_days);
        View repeatOn = mView.findViewById(R.id.b_repeat);
        View bRingtone = mView.findViewById(R.id.b_ringtone);
        View vibrateOn = mView.findViewById(R.id.b_vibrate);

        cbAlarmOn = (CheckBox) mView.findViewById(R.id.cb_alarmOn);
        tvTime = (TextView) mView.findViewById(R.id.tv_time);
        tvDays = (TextView) mView.findViewById(R.id.tv_days);
        cbRepeatOn = (CheckBox) mView.findViewById(R.id.cb_repeatOn);
        bInterval = mView.findViewById(R.id.b_interval);
        tvInterval = (TextView) mView.findViewById(R.id.tv_repeat_intervall);
        bRepeatNum = mView.findViewById(R.id.b_repean_num);
        tvRepeatNum = (TextView) mView.findViewById(R.id.tv_repeat_num);
        tvRingtone = (TextView) mView.findViewById(R.id.tv_ringtone);
        sbVolume = (SeekBar) mView.findViewById(R.id.sb_volume);
        cbVibrateOn = (CheckBox) mView.findViewById(R.id.cb_vibrateOn);
        tvNote = (EditText) mView.findViewById(R.id.tv_note);

        Vibrator vibrator = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
        if (!vibrator.hasVibrator()) {
            vibrateOn.setVisibility(View.GONE);
            mView.findViewById(R.id.dv_vibrator).setVisibility(View.GONE);
        }

        alarmOn.setOnClickListener(this);
        bTime.setOnClickListener(this);
        bDays.setOnClickListener(this);
        bInterval.setOnClickListener(this);
        bRingtone.setOnClickListener(this);
        repeatOn.setOnClickListener(this);
        bRepeatNum.setOnClickListener(this);
        vibrateOn.setOnClickListener(this);
        cbAlarmOn.setOnCheckedChangeListener(this);
        cbRepeatOn.setOnCheckedChangeListener(this);
        cbVibrateOn.setOnCheckedChangeListener(this);
        mView.findViewById(R.id.b_cancel).setOnClickListener(this);
        mView.findViewById(R.id.b_delete).setOnClickListener(this);
        mView.findViewById(R.id.b_save).setOnClickListener(this);
        sbVolume.setMax(100);

        Bundle arguments = getArguments();

        if (arguments != null) {
            int id = arguments.getInt(Util.ALARM_ID, -1);
            showAlarm(id);
        }

        return mView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof SettingsCompleteListener) {
            listener = (SettingsCompleteListener) activity;
        } else {
            throw new ClassCastException(activity.toString() + " must implemenet OnSettingsDone");
        }
    }

    @Override
    public void onClick(View v) {
        tvNote.clearFocus();
        switch (v.getId()) {
            case R.id.enable_alarm:
                cbAlarmOn.setChecked(!cbAlarmOn.isChecked());
                break;
            case R.id.b_time:
                openTimeDialog();
                break;
            case R.id.b_days:
                openDaysDialog();
                break;
            case R.id.b_interval:
                openIntervalDialog();
                break;
            case R.id.b_repeat:
                cbRepeatOn.setChecked(!cbRepeatOn.isChecked());
                break;
            case R.id.b_repean_num:
                openRepeatDialog();
                break;
            case R.id.b_ringtone:
                openSoundVariantsDialog();
                break;
            case R.id.b_vibrate:
                cbVibrateOn.setChecked(!cbVibrateOn.isChecked());
                break;
            case R.id.b_cancel:
                listener.onSettingsDone(Util.ACTION_CANCEL, -1);
                break;
            case R.id.b_delete:
                if (alarm == null)
                    return;
                Intent mServiceIntent = new Intent(getActivity(), AlarmService.class);
                mServiceIntent.setAction(AlarmService.ACTION_DELETE);
                mServiceIntent.putExtra(AlarmService.ALARM_ID, alarm.getId());
                getActivity().startService(mServiceIntent);
                break;
            case R.id.b_save:
                if (!validate())
                    return;

                mServiceIntent = new Intent(getActivity(), AlarmService.class);
                mServiceIntent.setAction(AlarmService.ACTION_SAVE);
                mServiceIntent.putExtra(AlarmService.ALARM_ID, alarm.getId());
                mServiceIntent.putExtra(AlarmService.ALARM, alarm);
                getActivity().startService(mServiceIntent);

                closeKeyboard();

                if (alarm.isEnabled()) {
                    String timeBeforeTrigger = Util.showTimeToTrigger(getActivity(), alarm.getTimeToTrigger(), days);
                    Toast.makeText(getActivity(), timeBeforeTrigger, Toast.LENGTH_LONG).show();
                }

                break;
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.cb_alarmOn:
                alarm.setEnabled(isChecked);
                if (!isChecked)
                    wasDisabled = true;
                break;
            case R.id.cb_repeatOn:
                alarm.setRepeat(isChecked);
                setRepeatSettingsState(isChecked);
                break;
            case R.id.cb_vibrateOn:
                alarm.setVibrate(isChecked);
                break;
        }
    }

    // --- sync with alarms list fragment ---
    @Override
    public void onAlarmEnabled(boolean isEnabled, int id) {
        if (alarm.getId() == id) {
            alarm.setEnabled(isEnabled);
            cbAlarmOn.setChecked(isEnabled);
        }
    }

    @Override
    public void onAlarmSaved(int id) {
        if (alarm.getId() == -1) {
            alarm.setId(id);
        }
    }

    @Override
    public void onAlarmDeleted(int id) {
        if (alarm.getId() == id){
            listener.onSettingsDone(Util.ACTION_DELETE, id);
        }
    }
    // ---------------------------------------

    // ---- Dialog Listener Callbacks ------
    @Override
    public void onRingtoneOptionSelected() {
        openRingtoneDialog();
    }

    @Override
    public void onMusicOptionSelected() {
        openMusicDialog();
    }

    @Override
    public void onSoundSelected(int position) {
        alarm.setRingtone(alarmsUri[position]);
        String ringtoneName = alarmsName[position];
        tvRingtone.setText(ringtoneName);
    }

    @Override
    public void onDaysSelected(boolean[] daysToChoose) {
        Util.findDays(alarm, days, daysToChoose);
        String daysStr = Util.getDaysOfWeek(getActivity(), days);
        tvDays.setText(daysStr);
    }

    @Override
    public void onIntervalSelected(long intervalInMillis) {
        alarm.setTimeInterval(intervalInMillis);
        tvInterval.setText(Util.formatTimeInterval(getActivity(), alarm.getTimeInterval()));
    }

    @Override
    public void onRepeatNumSelected(int repeatNum) {
        alarm.setRepeatNum(repeatNum);
        tvRepeatNum.setText(Util.formatRepeatTimes(getActivity(), alarm.getRepeatNum()));
    }

    @Override
    public void onTimeSelected(long timeInMillis) {
        alarm.setTimeToTrigger(timeInMillis);
        tvTime.setText(Util.formatTime(alarm.getTimeToTrigger()));
    }

    @Override
    public void onPackageSelected(String packageName, String activityName) {  }
    // ---------------------------------

    public void showAlarm(int id) {
        days.clear();
        if (id == -1) {
            alarm = new Alarm();
        } else {
            alarm = dbHelper.getAlarm(id);
            if (alarm == null) return;
            alarm.fillDays();
            days = alarm.getDays();
        }
        fillData();
        wasDisabled = false;
    }

    private void fillData() {
        cbAlarmOn.setChecked(alarm.isEnabled());

        if (alarm.getTimeToTrigger() == 0) {
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.MINUTE, 1);
            long timeToTrigger = calendar.getTimeInMillis();
            alarm.setTimeToTrigger(timeToTrigger);
        }
        tvTime.setText(Util.formatTime(alarm.getTimeToTrigger()));

        tvDays.setText(Util.getDaysOfWeek(getActivity(), days));

        cbRepeatOn.setChecked(alarm.isRepeat());
        setRepeatSettingsState(alarm.isRepeat());

        tvInterval.setText(Util.formatTimeInterval(getActivity(), alarm.getTimeInterval()));

        tvRepeatNum.setText(Util.formatRepeatTimes(getActivity(), alarm.getRepeatNum()));

        String ringtoneName;

        if (alarm.getRingtone().isEmpty()) {
            ringtoneName = getString(R.string.none);
        } else {
            File file = new File(alarm.getRingtone());
            if (file.exists())
                ringtoneName = file.getName();
            else {
                ringtoneName = getString(R.string.none);
                alarm.setRingtone("");
            }
        }
        tvRingtone.setText(ringtoneName);

        sbVolume.setProgress(alarm.getVolume());

        cbVibrateOn.setChecked(alarm.isVibrate());

        tvNote.setText(alarm.getNote());
    }

    private boolean validate() {
        if (alarm == null)
            return false;

        if (!wasDisabled) {
            alarm.setEnabled(true);
            cbAlarmOn.setChecked(true);
        }

        alarm.setNote(tvNote.getText().toString());

        alarm.setVolume(sbVolume.getProgress());

        if (alarm.getRingtone() == null || alarm.getRingtone().isEmpty()) {
            Toast.makeText(getActivity(), getString(R.string.set_ringtone), Toast.LENGTH_SHORT).show();
            return false;
        }
        if (days.isEmpty()) {
            Toast.makeText(getActivity(), getString(R.string.choose_days), Toast.LENGTH_SHORT).show();
            return false;
        }

        alarm.setDays(days);
        return true;
    }


    // ---- open dialogs -----------------------
    private void openSoundVariantsDialog() {
        Boolean isSDPresent = android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);
        if(isSDPresent) {
            AlarmSoundVariantsDialog alarmSoundDialog = new AlarmSoundVariantsDialog();
            alarmSoundDialog.show(getActivity().getSupportFragmentManager(), AlarmSoundVariantsDialog.TAG);
        } else {
            openRingtoneDialog();
        }
    }

    private void openDaysDialog() {
        boolean[] selectedItems = new boolean[7];
        for (Day day : days){
            selectedItems[day.getIndex()] = true;
        }
        DaysDialog daysDialog = new DaysDialog();
        daysDialog.setSelectedItems(selectedItems);
        daysDialog.show(getActivity().getSupportFragmentManager(), DaysDialog.TAG);
    }

    private void openIntervalDialog() {
        IntervalDialog intervalDialog = new IntervalDialog();
        intervalDialog.setCurrentTimeInterval(alarm.getTimeInterval());
        intervalDialog.show(getActivity().getSupportFragmentManager(), IntervalDialog.TAG);
    }

    private void openRepeatDialog() {
        RepeatDialog repeatDialog = new RepeatDialog();
        repeatDialog.setRepeatNum(alarm.getRepeatNum());
        repeatDialog.show(getActivity().getSupportFragmentManager(), RepeatDialog.TAG);
    }

    private void openTimeDialog() {
        TimeDialog timeDialog = new TimeDialog();
        timeDialog.setTimeToTrigger(alarm.getTimeToTrigger());
        timeDialog.show(getActivity().getSupportFragmentManager(), TimeDialog.TAG);
    }

    private void openMusicDialog() {
        type = 1;
        getMusicFromSDCard();
    }

    private void openRingtoneDialog() {
        type = 0;
        getMusicFromSDCard();
    }

    private void getMusicFromSDCard() {
        progressDialog = new ProgressDialog();
        progressDialog.setType(type);
        progressDialog.show(getActivity().getSupportFragmentManager(), ProgressDialog.TAG);

        getActivity().getSupportLoaderManager().initLoader(type, null, this);
    }

    int type = 0;

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        String[] projection = { MediaStore.Audio.Media._ID, MediaStore.Audio.Media.DATA,  MediaStore.Audio.Media.TITLE};

        CursorLoader cursorLoader;

        if (type == 0) {
            List<String> mFilterColumns = new ArrayList<String>();
            mFilterColumns.add(MediaStore.Audio.AudioColumns.IS_ALARM);
            cursorLoader = new CursorLoader(getActivity(),
                    MediaStore.Audio.Media.INTERNAL_CONTENT_URI,
                    projection,
                    constructBooleanTrueWhereClauseForMusic(mFilterColumns),
                    null, MediaStore.Audio.Media.DEFAULT_SORT_ORDER);
        } else {
            List<String> mFilterColumns = new ArrayList<String>();
            mFilterColumns.add(MediaStore.Audio.AudioColumns.IS_MUSIC);
            cursorLoader = new CursorLoader(getActivity(),
                    MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                    projection,
                    constructBooleanTrueWhereClauseForMusic(mFilterColumns),
                    null, null);
        }
        return cursorLoader;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        if (cursor == null || !cursor.moveToFirst()) {
            return;
        }

        fillMusic(cursor);

        if (alarmsName == null || alarmsName.length == 0||
                alarmsUri == null || alarmsUri.length == 0) {
            Toast.makeText(getActivity(), getString(R.string.no_music_found), Toast.LENGTH_SHORT).show();
            return;
        }

        if (!alarm.getRingtone().isEmpty()) {
            for (int i = 0; i < alarmsUri.length; i++) {
                if (alarmsUri[i].equals(alarm.getRingtone())) {
                    checkedMusicItem = i;
                    break;
                }
            }
        }

        musicDialogHandler.sendEmptyMessage(2);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {}

    private Handler musicDialogHandler = new Handler()  {
        @Override
        public void handleMessage(Message msg) {
            if(msg.what == 2) {
                if (progressDialog != null)
                    progressDialog.dismiss();

                AlarmSoundDialog alarmSoundDialog = new AlarmSoundDialog();
                alarmSoundDialog.setSoundNames(alarmsName);
                alarmSoundDialog.setSelectedItem(checkedMusicItem);
                alarmSoundDialog.show(getActivity().getSupportFragmentManager(), AlarmSoundDialog.TAG);
            }
        }
    };

    private void fillMusic(Cursor cursor) {
        int alarmsCount = cursor.getCount();
        alarmsUri = new String[alarmsCount];
        alarmsName = new String[alarmsCount];
        while(!cursor.isAfterLast()) {
            int currentPosition = cursor.getPosition();
            alarmsUri[currentPosition] = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DATA));
            alarmsName[currentPosition] = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.TITLE));
            cursor.moveToNext();
        }
    }
    //--------------------------

    private void setRepeatSettingsState(boolean isChecked) {
        if (isChecked) {
            bInterval.setEnabled(true);
            bInterval.setAlpha(1f);
            bRepeatNum.setEnabled(true);
            bRepeatNum.setAlpha(1f);
        } else {
            bInterval.setEnabled(false);
            bInterval.setAlpha(0.7f);
            bRepeatNum.setEnabled(false);
            bRepeatNum.setAlpha(0.7f);
        }
    }

    private void closeKeyboard() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(tvNote.getWindowToken(), 0);
        tvNote.clearFocus();
    }

    private static String constructBooleanTrueWhereClauseForMusic(List<String> columns) {

        if (columns == null) return null;

        StringBuilder sb = new StringBuilder();
        sb.append("(");

        for (int i = columns.size() - 1; i >= 0; i--) {
            sb.append(columns.get(i)).append("=1 or ");
        }

        if (columns.size() > 0) {
            sb.setLength(sb.length() - 4);
        }

        sb.append(")");

        return sb.toString();
    }
}
