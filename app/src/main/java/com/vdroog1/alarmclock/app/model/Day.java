package com.vdroog1.alarmclock.app.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by kettricken on 04.11.2014.
 */
@DatabaseTable(tableName = "days")
public class Day implements Parcelable, Comparable<Day> {

    @DatabaseField(generatedId = true, columnDefinition = "INTEGER PRIMARY KEY")
    int id;

    @DatabaseField(foreign=true, foreignAutoRefresh=true, uniqueCombo = true)
    Alarm alarm;

    @DatabaseField(dataType = DataType.INTEGER, uniqueCombo = true)
    int dayOfWeek;

    @DatabaseField(dataType = DataType.INTEGER)
    int dayIndex;

    public Day() {}

    public Day(Alarm alarm, int i) {
        this.alarm = alarm;
        this.dayOfWeek = i;
    }

    public int getDayOfWeek() {
        return dayOfWeek;
    }

    public int getId() {
        return id;
    }

    public void setDayOfWeek(int dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public int getIndex() {
        return dayIndex;
    }

    public void setIndex(int index) {
        this.dayIndex = index;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(dayOfWeek);
        dest.writeInt(dayIndex);
    }

    private Day(Parcel in) {
        id = in.readInt();
        dayOfWeek = in.readInt();
        dayIndex = in.readInt();
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Day createFromParcel(Parcel in) {
            return new Day(in);
        }

        public Day[] newArray(int size) {
            return new Day[size];
        }

    };

    public void setAlarm(Alarm alarm) {
        this.alarm = alarm;
    }

    @Override
    public int compareTo(Day another) {
        if (dayIndex == another.getIndex())
            return 0;
        else if (dayIndex < another.getIndex()) {
            return -1;
        }
        return 1;
    }
}
