package com.vdroog1.alarmclock.app.interfaces;

/**
 * Created by kettricken on 29.11.2014.
 */
public interface AlarmServiceListener {
    public void onAlarmEnabled(boolean isEnabled, int id);
    public void onAlarmSaved(int id);
    public void onAlarmDeleted(int id);
}