package com.vdroog1.alarmclock.app.DB;

import android.content.Context;
import com.j256.ormlite.android.apptools.OpenHelperManager;

/**
 * Created by KaterinaG on 04.11.2014.
 * Helper for DB lifecycle
 */
public class HelperFactory {

    private static DataBaseHelper databaseHelper;

    public static DataBaseHelper getHelper(){
        return databaseHelper;
    }
    public static void setHelper(Context context){
        databaseHelper = OpenHelperManager.getHelper(context, DataBaseHelper.class);
    }
    public static void releaseHelper(){
        OpenHelperManager.releaseHelper();
        databaseHelper = null;
    }

}
