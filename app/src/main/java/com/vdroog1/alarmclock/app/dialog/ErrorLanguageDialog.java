package com.vdroog1.alarmclock.app.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import com.vdroog1.alarmclock.app.R;

/**
 * Created by KaterinaG on 04.12.2014.
 * Dialog to show that the language is not selected
 */
public class ErrorLanguageDialog extends SettingsDialog {

    public static final String TAG = ErrorLanguageDialog.class.getSimpleName();

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.error_no_language)
                .setNegativeButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        return  builder.create();
    }
}
