package com.vdroog1.alarmclock.app.adapter;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by KaterinaG on 12.11.2014.
 * Adapter for package chooser
 */
public class ChooserArrayAdapter extends ArrayAdapter<String> {

    PackageManager packageManager;
    int mTextViewResourceId;
    List<String> mPackages;

    public ChooserArrayAdapter(Context context, int resource, int textViewResourceId, List<String> packages) {
        super(context, resource, textViewResourceId, packages);
        packageManager = context.getPackageManager();
        mTextViewResourceId = textViewResourceId;
        mPackages = packages;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        String packageName = mPackages.get(position);
        View view = super.getView(position, convertView, parent);

        try {
            ApplicationInfo applicationInfo = packageManager.getApplicationInfo(packageName, 0);

            CharSequence appName = packageManager.getApplicationLabel(applicationInfo);
            Drawable appIcon = packageManager.getApplicationIcon(packageName);

            int padding = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 12,
                    getContext().getResources().getDisplayMetrics());

            TextView textView = (TextView) view.findViewById(mTextViewResourceId);
            textView.setText(appName);
            textView.setCompoundDrawablesWithIntrinsicBounds(appIcon, null, null, null);
            textView.setCompoundDrawablePadding(padding);

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        return view;
    }

}
