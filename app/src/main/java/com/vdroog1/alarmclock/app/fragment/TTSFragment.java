package com.vdroog1.alarmclock.app.fragment;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.Fragment;
import com.vdroog1.alarmclock.app.service.TTSEngine;

import java.util.ArrayList;

/**
 * Created by KaterinaG on 30.11.2014.
 * Fragment to handle TTS check for existence
 */
public abstract class TTSFragment extends Fragment implements TTSEngine.TtsEngineListener {

    @Override
    public abstract void onTTSInit();

    @Override
    public abstract void onTTSPlayCompleted();

    @Override
    public abstract void onTTSPlayStarted();

    @Override
    public abstract void startChooserDialog(ArrayList<String> packages, ArrayList<String> packageActivities);

    @Override
    public void invokeApplication(Context context, String packageName, String className) {
        Intent launchIntent = getActivity().getPackageManager().getLaunchIntentForPackage(packageName);
        if (launchIntent != null) {
            launchIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
            startActivityForEngine(launchIntent);
        } else {
            Intent intent = new Intent();
            ComponentName comp = new ComponentName(packageName, className);
            intent.setComponent(comp);
            intent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
            startActivityForEngine(intent);
        }
    }

    public void startActivityForEngine(Intent intent) {
        startActivityForResult(intent, TTSEngine.MY_DATA_CHECK_CODE);
    }
}
