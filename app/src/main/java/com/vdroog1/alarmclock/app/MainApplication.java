package com.vdroog1.alarmclock.app;

import android.app.Application;
import com.vdroog1.alarmclock.app.DB.HelperFactory;

/**
 * Created by kettricken on 04.11.2014.
 */
public class MainApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        HelperFactory.setHelper(getApplicationContext());
    }
    @Override
    public void onTerminate() {
        HelperFactory.releaseHelper();
        super.onTerminate();
    }
}
