package com.vdroog1.alarmclock.app.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.vdroog1.alarmclock.app.DB.DataBaseHelper;
import com.vdroog1.alarmclock.app.DB.HelperFactory;
import com.vdroog1.alarmclock.app.R;
import com.vdroog1.alarmclock.app.Util;
import com.vdroog1.alarmclock.app.model.Alarm;
import com.vdroog1.alarmclock.app.service.AlarmService;
import com.vdroog1.alarmclock.app.service.TTSConcat;
import com.vdroog1.alarmclock.app.service.TTSEngine;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by KaterinaG on 29.11.2014.
 * Activity to stop the alarm
 */
public class StopFragment extends TTSFragment implements View.OnClickListener {

    public static final String TAG = StopFragment.class.getSimpleName();

    private final long RING_TIME = 60 * 1000;
    private final long TIME = 10 * 1000;

    Alarm alarm;

    TextView tvNote;
    View bPostpone;
    View bDismiss;

    MediaPlayer mMediaPlayer;
    Vibrator vibrator;
    Handler handler = new Handler();
    Runnable stopMusic = new Runnable() {
        @Override
        public void run() {
            AlarmService.addLog("StopFragment: Time has passed");
            postpone();
        }
    };

    Runnable playTime = new Runnable() {
        @Override
        public void run() {
            sayTime();
        }
    };

    SharedPreferences prefs;
    Locale locale;
    boolean isTTS;
    boolean isMale;
    float volume = 1f;
    float halfVolume = 0.3f;

    TTSEngine ttsEngine;
    TTSConcat ttsConcat;

    int alarmId;
    int dayId;

    boolean isTTSEngineAvailable = false;

    AudioManager mAudioManager;
    int origionalVolume;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AlarmService.addLog("StopFragment: onCreate");

        setRetainInstance(true);

        HelperFactory.setHelper(getActivity());
        DataBaseHelper dbHelper = HelperFactory.getHelper();

        prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        isTTS = prefs.getBoolean(Util.TTS_ENABLED, false);
        String localeStr = prefs.getString(Util.LOCALE, "en");
        locale = new Locale(localeStr);
        isMale = prefs.getBoolean(Util.IS_MALE, false);
        alarmId = getArguments().getInt(AlarmService.ALARM_ID, -1);
        dayId = getArguments().getInt(AlarmService.DAY_ID, -1);

        alarm = dbHelper.getAlarm(alarmId);
        if (alarm == null) return;

        AlarmService.addLog("StopFragment: alarm: " + alarm.toString());
        volume = alarm.getVolume() / 100f;
        halfVolume = volume / 10f;

        try {
            if (isTTS) {
                ttsEngine = new TTSEngine(this, false);
                ttsEngine.enable(getActivity());
                ttsEngine.setVolume(volume);
                isTTSEngineAvailable = prefs.getBoolean(TTSEngine.IS_ENABLED, false);
                if (isTTSEngineAvailable)
                    ttsEngine.forceInit(getActivity());
            } else {
                ttsConcat = new TTSConcat(this);
                ttsConcat.setVolume(volume);
            }

            mAudioManager = ((AudioManager) getActivity().getSystemService(Context.AUDIO_SERVICE));
            origionalVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
            mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC), 0);

            Uri alert;
            if (alarm.getRingtone().isEmpty() || alarm.getRingtone() == null) {
                alert = Settings.System.DEFAULT_RINGTONE_URI;
            } else {
                alert = Uri.parse(alarm.getRingtone());
            }
            mMediaPlayer = new MediaPlayer();
            mMediaPlayer.setDataSource(getActivity(), alert);
            mMediaPlayer.setVolume(volume, volume);
            mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mMediaPlayer.setLooping(true);
            mMediaPlayer.prepare();
            mMediaPlayer.start();

            if (alarm.isVibrate()) {
                vibrator = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
                long[] pattern = {0, 1000, 600};
                vibrator.vibrate(pattern, 0);
            }

            handler.postDelayed(stopMusic, RING_TIME);
            handler.postDelayed(playTime, TIME);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.stop_activity, container, false);

        tvNote = (TextView) mView.findViewById(R.id.tv_note);
        bPostpone = mView.findViewById(R.id.b_postpone);
        bDismiss = mView.findViewById(R.id.b_dismiss);
        bPostpone.setOnClickListener(this);
        bDismiss.setOnClickListener(this);

        if (!alarm.isRepeat())
            bPostpone.setVisibility(View.GONE);

        tvNote.setText(alarm.getNote());

        return mView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("Test", "StopFragment:onActivityResult");
        isTTSEngineAvailable = ttsEngine.onActivityResult(getActivity().getApplicationContext(), requestCode, resultCode);
        AlarmService.addLog("StopFragment:onActivityResult got: isTTSEngineAvailable " + isTTSEngineAvailable);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.b_postpone:
                AlarmService.addLog("StopFragment: Postpone is clicked");
                postpone();
                break;
            case R.id.b_dismiss:
                AlarmService.addLog("StopFragment: Dismiss is clicked");
                stopPlayer();
                stopRepeating(true);
                getActivity().finish();
                break;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (ttsEngine != null) ttsEngine.onFinish();
        if (ttsConcat != null) ttsConcat.onFinish();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isTTS && isTTSEngineAvailable) {
            ttsEngine.enable(getActivity());
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d("Test", "StopFragment:onPause");
        AlarmService.addLog("StopFragment:onPause");
    }

    @Override
    public void onTTSInit() {  }

    @Override
    public void onTTSError() {

    }

    @Override
    public void onTTSPlayCompleted() {
        if (mMediaPlayer != null && mMediaPlayer.isPlaying())
            mMediaPlayer.setVolume(volume, volume);
    }

    @Override
    public void onTTSPlayStarted() {
        if (mMediaPlayer != null && mMediaPlayer.isPlaying())
            mMediaPlayer.setVolume(halfVolume, halfVolume);
    }

    @Override
    public void invokeApplication(Context context, String packageName, String className) {
        super.invokeApplication(context, packageName, className);
    }

    @Override
    public void startChooserDialog(ArrayList<String> packages, ArrayList<String> packageActivities) {

    }

    @Override
    public void onTTSLanguageError() {

    }

    public void onBackPressed() {
        postpone();
    }

    private void sayTime() {
        AlarmService.addLog("StopFragment:sayTime: isTts " + isTTS + " isTTSEngineAvailable " + isTTSEngineAvailable);
        if (isTTS && isTTSEngineAvailable) {
            Calendar calendar = Calendar.getInstance();
            String str = DateUtils.formatDateTime(getActivity(), calendar.getTimeInMillis(), DateUtils.FORMAT_SHOW_TIME);
            ttsEngine.say(locale, str);
        } else if (!isTTS) {
            if (!(locale.getLanguage().contentEquals("ru") || locale.getLanguage().contentEquals("en"))) {
                locale = new Locale("en");
            }
            Calendar calendar = Calendar.getInstance();
            ttsConcat.pronounceTime(getActivity(), locale, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), isMale);
        }
        handler.postDelayed(playTime, TIME);
    }

    private void stopRepeating(boolean isDismissed) {
        Intent mServiceIntent = new Intent(getActivity(), AlarmService.class);
        mServiceIntent.setAction(AlarmService.ACTION_REPEAT);
        mServiceIntent.putExtra(AlarmService.ALARM_ID, alarm.getId());
        mServiceIntent.putExtra(AlarmService.DAY_ID, dayId);
        mServiceIntent.putExtra(AlarmService.DISMISSED, isDismissed);
        getActivity().startService(mServiceIntent);
    }

    private void postpone() {
        stopPlayer();
        if (alarm.isRepeat())
            stopRepeating(false);
        else
            stopRepeating(true);
        getActivity().finish();
    }

    private void stopPlayer() {
        handler.removeCallbacks(stopMusic);
        handler.removeCallbacks(playTime);
        if (vibrator != null) vibrator.cancel();
        if (mMediaPlayer != null && mMediaPlayer.isPlaying()) {
            mMediaPlayer.setLooping(false);
            mMediaPlayer.stop();
            mMediaPlayer.reset();
            mMediaPlayer.release();
            mMediaPlayer = null;
            mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, origionalVolume, 0);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mMediaPlayer != null && mMediaPlayer.isPlaying())
            postpone();
    }
}
