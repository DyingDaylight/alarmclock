package com.vdroog1.alarmclock.app.dialog;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import com.vdroog1.alarmclock.app.fragment.SettingsFragment;
import com.vdroog1.alarmclock.app.fragment.TTSSettingsFragment;
import com.vdroog1.alarmclock.app.interfaces.SettingsDialogsListener;

/**
 * Created by KaterinaG on 01.12.2014.
 * Base dialog for all settings dialogs
 */
public class SettingsDialog extends DialogFragment {

    SettingsDialogsListener listener;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SettingsFragment settingsFragment = (SettingsFragment) getActivity().getSupportFragmentManager()
                .findFragmentByTag(SettingsFragment.TAG);

        if (settingsFragment == null) {
            TTSSettingsFragment ttsSettingsFragment = (TTSSettingsFragment) getActivity().getSupportFragmentManager()
                    .findFragmentByTag(TTSSettingsFragment.TAG);
            listener = ttsSettingsFragment;
            return;
        }

        listener = settingsFragment;
    }
}
