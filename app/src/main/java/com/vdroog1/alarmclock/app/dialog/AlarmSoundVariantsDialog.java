package com.vdroog1.alarmclock.app.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import com.vdroog1.alarmclock.app.R;

/**
 * Created by KaterinaG on 02.12.2014.
 * Dialog to choose between music from SDCard and a ringtone
 */
public class AlarmSoundVariantsDialog extends SettingsDialog {

    public static final String TAG = AlarmSoundVariantsDialog.class.getSimpleName();

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.set_ringtone_title)
                .setPositiveButton(R.string.ringtone, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        listener.onRingtoneOptionSelected();
                        dialog.dismiss();
                    }
                })
                .setNegativeButton(R.string.music, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        listener.onMusicOptionSelected();
                        dialog.dismiss();
                    }
                });
        return builder.create();
    }
}
