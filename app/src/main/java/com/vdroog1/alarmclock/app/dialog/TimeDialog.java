package com.vdroog1.alarmclock.app.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.WindowManager;
import android.widget.TimePicker;
import com.vdroog1.alarmclock.app.R;

import java.util.Calendar;

/**
 * Created by KaterinaG on 02.12.2014.
 * Dialog to choose time of the alarm
 */
public class TimeDialog extends SettingsDialog {

    public static final String TAG = TimeDialog.class.getSimpleName();

    TimePicker timePicker;

    long timeToTrigger;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null){
            timeToTrigger = savedInstanceState.getLong("timeToTrigger");
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Calendar time = Calendar.getInstance();
        time.set(Calendar.HOUR_OF_DAY, timePicker.getCurrentHour());
        time.set(Calendar.MINUTE, timePicker.getCurrentMinute());
        timeToTrigger = time.getTimeInMillis();
        outState.putLong("timeToTrigger", timeToTrigger);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Calendar time = Calendar.getInstance();
        time.setTimeInMillis(timeToTrigger);

        timePicker = new TimePicker(getActivity());
        timePicker.setIs24HourView(true);
        timePicker.setCurrentHour(time.get(Calendar.HOUR_OF_DAY));
        timePicker.setCurrentMinute(time.get(Calendar.MINUTE));

        Dialog dialog = new AlertDialog.Builder(getActivity())
                .setTitle(getString(R.string.set_time))
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Calendar time = Calendar.getInstance();
                        time.set(Calendar.HOUR_OF_DAY, timePicker.getCurrentHour());
                        time.set(Calendar.MINUTE, timePicker.getCurrentMinute());
                        listener.onTimeSelected(time.getTimeInMillis());
                        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                    }
                })
                .setNegativeButton(getString(R.string.cancel),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                            }
                        })
                .setView(timePicker)
                .show();
        return dialog;
    }

    public void setTimeToTrigger(long timeToTrigger){
        this.timeToTrigger = timeToTrigger;
    }
}
