package com.vdroog1.alarmclock.app.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.NumberPicker;
import com.vdroog1.alarmclock.app.R;
import com.vdroog1.alarmclock.app.Util;

/**
 * Created by KaterinaG on 02.12.2014.
 * Dialog to choose rings intervals
 */
public class IntervalDialog extends SettingsDialog {

    public static final String TAG = IntervalDialog.class.getSimpleName();

    private NumberPicker numberPicker;

    private long currentTimeInterval;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            currentTimeInterval = savedInstanceState.getLong("currentTimeInterval");
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        currentTimeInterval = numberPicker.getValue() * 60 * 1000;
        outState.putLong("currentTimeInterval", currentTimeInterval);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view  = layoutInflater.inflate(R.layout.number_picker, null, false);
        numberPicker = (NumberPicker) view.findViewById(R.id.numberPicker1);
        numberPicker.setMaxValue(30);
        numberPicker.setMinValue(3);
        numberPicker.setWrapSelectorWheel(true);
        numberPicker.setValue(Util.timeIntervalInMinutes(currentTimeInterval));
        numberPicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);

        return new AlertDialog.Builder(getActivity())
                .setTitle(getString(R.string.set_interval))
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        int intervalInMinutes = numberPicker.getValue();
                        long intervalInMillis = intervalInMinutes * 60 * 1000;
                        listener.onIntervalSelected(intervalInMillis);
                        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                    }
                })
                .setNegativeButton(getString(R.string.cancel),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                            }
                        })
                .setView(view)
                .show();
    }

    public void setCurrentTimeInterval(long currentTimeInterval){
        this.currentTimeInterval = currentTimeInterval;
    }
}
