package com.vdroog1.alarmclock.app.interfaces;

/**
 * Created by kettricken on 02.12.2014.
 */
public interface SettingsDialogsListener {

    public void onRingtoneOptionSelected();
    public void onMusicOptionSelected();

    public void onSoundSelected(int position);

    public void onDaysSelected(boolean[] daysToChoose);

    public void onIntervalSelected(long intervalInMillis);

    public void onRepeatNumSelected(int repeatNum);

    public void onTimeSelected(long timeInMillis);

    public void onPackageSelected(String packageName, String activityName);
}
