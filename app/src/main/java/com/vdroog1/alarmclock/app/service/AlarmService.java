package com.vdroog1.alarmclock.app.service;

import android.app.*;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;
import com.vdroog1.alarmclock.app.DB.DataBaseHelper;
import com.vdroog1.alarmclock.app.DB.HelperFactory;
import com.vdroog1.alarmclock.app.R;
import com.vdroog1.alarmclock.app.Util;
import com.vdroog1.alarmclock.app.model.Alarm;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

/**
 * Created by kettricken on 03.11.2014.
 */
public class AlarmService extends Service {

    public static final String ACTION_SAVE = "ACTION_SAVE";
    public static final String ACTION_DELETE = "ACTION_DELETE";
    public static final String ACTION_STOP = "ACTION_STOP";
    public static final String ACTION_CHANGE_STATE = "ACTION_CHANGE_STATE";
    public static final String ALARM_ID = "ALARM_ID";
    public static final String ALARM = "ALARM";
    public static final String ENABLED = "ENABLED";
    public static final String UPDATE = "UPDATE";
    public static final long INTERVAL_DAY = AlarmManager.INTERVAL_DAY * 7;
    public static final String DAY_ID = "day_id";
    public static final String ACTION_REPEAT = "ACTION_REPEAT";
    public static final String DISMISSED = "DISMISSED";
    public static final String ACTION_START = "ACTION_START";

    IntentFilter filter;

    HashMap<Integer, Runnable> repeatingAlarm = init();

    private HashMap<Integer, Runnable> init() {
        return new HashMap<Integer, Runnable>();
    }

    Handler repeatHandler = new Handler();

    public AlarmService() {
        super();
        filter = new IntentFilter();
        filter.addAction(ACTION_SAVE);
        filter.addAction(ACTION_DELETE);
        filter.addAction(ACTION_CHANGE_STATE);
        filter.addAction(ACTION_REPEAT);
        filter.addAction(ACTION_STOP);
        filter.addAction(ACTION_START);
    }

    private static String string = "";

    public static void addLog(String s) {
        string += "\n" + s;
    }

    public void onCreate() {
        super.onCreate();
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        onHandleIntent(intent);
        return START_FLAG_REDELIVERY;
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    protected void onHandleIntent(Intent intent) {
        if (intent == null)
            return;

        String action = intent.getAction();
        int alarmId = intent.getIntExtra(ALARM_ID, -1);
        int dayId = intent.getIntExtra(DAY_ID, -1);
        Alarm alarm = intent.getParcelableExtra(ALARM);
        boolean isEnabled = intent.getBooleanExtra(ENABLED, false);
        boolean isDismissed = intent.getBooleanExtra(DISMISSED, true);

        if (filter.hasAction(action)) {
            if (action.equals(ACTION_REPEAT)) {
                executeRepeat(alarmId, dayId, isDismissed);
            } else if (action.equals(ACTION_STOP)) {
                executeStop(dayId);
            } else if (action.equals(ACTION_START)) {
                executeStart();
            } else {
                executeAction(action, alarmId, alarm, isEnabled);
            }
        }
    }

    private void executeStart() {
        DataBaseHelper dbHelper = HelperFactory.getHelper();
        List<Alarm> alarms =  dbHelper.getAlarmsList();
        for (Alarm alarm : alarms) {
            if (alarm.isEnabled()) {
                alarm.fillDays();
                startAlarm(alarm);
            }
        }
    }

    private void executeStop(int dayId) {
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent alarmIntent = new Intent(this, AlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, dayId, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        alarmManager.cancel(pendingIntent);
        writeToFile(getBaseContext());
    }

    private void executeRepeat(int alarmId, int dayId, boolean isDismissed) {
        if (isDismissed) {
            stopAlarmRepeating(alarmId);
            writeToFile(getBaseContext());
            return;
        }

        DataBaseHelper dbHelper = HelperFactory.getHelper();
        Alarm alarm = dbHelper.getAlarm(alarmId);

        Calendar alarmStartTime = Calendar.getInstance();
        alarmStartTime.setTimeInMillis(alarm.getTimeToTrigger());

        Calendar alarmStartTimeToday = Calendar.getInstance();
        alarmStartTimeToday.set(Calendar.HOUR_OF_DAY, alarmStartTime.get(Calendar.HOUR_OF_DAY));
        alarmStartTimeToday.set(Calendar.MINUTE, alarmStartTime.get(Calendar.MINUTE));

        long timesCalled = (System.currentTimeMillis() - alarmStartTimeToday.getTimeInMillis()) / alarm.getTimeInterval();

        if (timesCalled < alarm.getRepeatNum() - 1) {
            startAlarmRepeat(alarm, dayId);
        } else {
            stopAlarmRepeating(alarmId);
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
            String time = sdf.format(alarmStartTimeToday.getTimeInMillis());
            sendNotification(time, alarmId);
        }

    }

    private void sendNotification(String time, int alarmId) {
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(getString(R.string.missed_call) + " " + time);
        Notification notification = mBuilder.build();
        NotificationManager mNotifyMgr =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        mNotifyMgr.notify(alarmId, notification);
    }

    private void startAlarmRepeat(final Alarm alarm, final int dayId) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                Intent alarmIntent = new Intent(AlarmService.this, AlarmReceiver.class);
                alarmIntent.putExtra(ALARM_ID, alarm.getId());
                alarmIntent.putExtra(DAY_ID, dayId);
                sendBroadcast(alarmIntent);
            }
        };
        repeatingAlarm.put(alarm.getId(), runnable);

        Calendar alarmStartTime = Calendar.getInstance();
        alarmStartTime.setTimeInMillis(alarm.getTimeToTrigger());

        Calendar alarmStartTimeToday = Calendar.getInstance();
        alarmStartTimeToday.set(Calendar.HOUR_OF_DAY, alarmStartTime.get(Calendar.HOUR_OF_DAY));
        alarmStartTimeToday.set(Calendar.MINUTE, alarmStartTime.get(Calendar.MINUTE));

        int timesCalled = (int) ((System.currentTimeMillis() - alarmStartTimeToday.getTimeInMillis()) / alarm.getTimeInterval());
        int nexTime = (int) timesCalled + 1;
        alarmStartTimeToday.add(Calendar.MINUTE, nexTime * (Util.timeIntervalInMinutes(alarm.getTimeInterval())));
        long dif = alarmStartTimeToday.getTimeInMillis() - System.currentTimeMillis();

        repeatHandler.postDelayed(runnable, dif);

    }

    private void stopAlarmRepeating(int alarmId) {
        if (repeatingAlarm.containsKey(alarmId)) {
            Runnable runnable = repeatingAlarm.get(alarmId);
            if (runnable != null) {
                repeatHandler.removeCallbacks(runnable);
                repeatingAlarm.put(alarmId, null);
            }
        }
    }

    private void executeAction(String action, int alarmId, Alarm newAlarm, boolean isEnabled) {
        DataBaseHelper dbHelper = HelperFactory.getHelper();
        Alarm oldAlarm = dbHelper.getAlarm(alarmId);

        stopAlarmRepeating(alarmId);

        if (action.equals(ACTION_CHANGE_STATE)) {
            if (oldAlarm == null)
                return;

            oldAlarm.setEnabled(isEnabled);
            dbHelper.saveAlarm(oldAlarm);
            oldAlarm.fillDays();
            if (isEnabled) startAlarm(oldAlarm);
            else stopAlarm(oldAlarm);
            Intent intent = new Intent(UPDATE);
            intent.putExtra(Util.ALARM_ID, oldAlarm.getId());
            intent.putExtra(Util.ACTION, Util.ACTION_UPDATE);
            intent.putExtra(Util.ENABLED, isEnabled);
            sendBroadcast(intent);
            return;
        }

        if (oldAlarm != null) {
            oldAlarm.fillDays();
            stopAlarm(oldAlarm);
        }

        if (action.equals(ACTION_DELETE)) {
            Intent intent = new Intent(UPDATE);
            intent.putExtra(Util.ALARM_ID, oldAlarm.getId());
            intent.putExtra(Util.ACTION, Util.ACTION_DELETE);
            dbHelper.deleteAlarm(oldAlarm);
            sendBroadcast(intent);
            return;
        }

        if (action.equals(ACTION_SAVE)) {
            dbHelper.saveAlarm(newAlarm);
            newAlarm.setDaysOfWeek();

            if (newAlarm.isEnabled())
                startAlarm(newAlarm);

            Intent intent = new Intent(UPDATE);
            intent.putExtra(Util.ALARM_ID, newAlarm.getId());
            intent.putExtra(Util.ACTION, Util.ACTION_SAVE);
            sendBroadcast(intent);

            return;
        }
    }

    private void startAlarm(Alarm alarm) {
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent alarmIntent = new Intent(this, AlarmReceiver.class);
        for (int i = 0; i < alarm.getDays().size(); i++) {
            alarmIntent.putExtra(DAY_ID, alarm.getDays().get(i).getId());
            alarmIntent.putExtra(ALARM_ID, alarm.getId());
            PendingIntent pendingIntent = PendingIntent.getBroadcast(this, alarm.getDays().get(i).getId(), alarmIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT);
            Calendar alarmCalendar = Calendar.getInstance();
            alarmCalendar.setTimeInMillis(alarm.getTimeToTrigger());
            alarmCalendar.set(Calendar.SECOND, 0);
            alarmCalendar.set(Calendar.MILLISECOND, 0);
            alarmCalendar.set(Calendar.DAY_OF_WEEK, alarm.getDays().get(i).getDayOfWeek());

            Calendar todayCalendar = Calendar.getInstance();

            Util.ifTriggerBeforeToday(todayCalendar, alarmCalendar, alarm.getDays().get(i));

            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, alarmCalendar.getTimeInMillis(), INTERVAL_DAY,
                    pendingIntent);
        }
    }

    private void stopAlarm(Alarm alarm) {
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent alarmIntent = new Intent(this, AlarmReceiver.class);
        for (int i = 0; i < alarm.getDays().size(); i++) {
            alarmIntent.putExtra("id", alarm.getDays().get(i).getId());
            PendingIntent pendingIntent = PendingIntent.getBroadcast(this, alarm.getDays().get(i).getId(), alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            alarmManager.cancel(pendingIntent);
        }
    }

    private void writeToFile(Context baseContext) {
        File root = Environment.getExternalStorageDirectory();
        try {
            File outputFile = new File(root, "AlarmClockLog.txt");
            BufferedWriter writer = new BufferedWriter(new FileWriter(outputFile, true));
            writer.write(string);
            Toast.makeText(baseContext.getApplicationContext(),
                    "Report successfully saved to: " + outputFile.getAbsolutePath(),
                    Toast.LENGTH_LONG).show();
            writer.close();
        } catch (IOException e) {
            Log.w("eztt", e.getMessage(), e);
            Toast.makeText(baseContext, e.getMessage() + " Unable to write to external storage.",
                    Toast.LENGTH_LONG).show();
        }
        string = "=============================================";
    }
}
