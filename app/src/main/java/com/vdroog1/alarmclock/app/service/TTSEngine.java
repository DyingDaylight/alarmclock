package com.vdroog1.alarmclock.app.service;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ResolveInfo;
import android.os.Build;
import android.preference.PreferenceManager;
import android.speech.tts.TextToSpeech;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Created by kettricken on 11.11.2014.
 */
public class TTSEngine implements TextToSpeech.OnInitListener {

    public static final int MY_DATA_CHECK_CODE = 101;
    public static final String TTS_PACKAGE = "tts_package";
    public static final String TTS_ACTIVITY = "tts_activity";
    public static final String IS_ENABLED = "IS_ENABLED";
    TtsEngineListener listener;

    private TextToSpeech tts;
    Locale locale;

    boolean isSettingsRun = true;

    float volume = 1;

    boolean isEnabled = false;

    ArrayList<Locale> localesList = new ArrayList<Locale>();

    public void setVolume(float volume) {
        this.volume = volume;
    }

    public void forceInit(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String packageName = prefs.getString(TTS_PACKAGE, "");
        if (Build.VERSION.SDK_INT >= 14)
            tts = new TextToSpeech(context, this, packageName);
        else
            tts = new TextToSpeech(context, this);
    }

    public interface TtsEngineListener {
        public void onTTSInit();
        public void onTTSError();
        public void onTTSPlayCompleted();
        public void onTTSPlayStarted();
        public void invokeApplication(Context context, String packageName, String className);
        public void startChooserDialog(ArrayList<String> packages, ArrayList<String> packageActivities);

        public void onTTSLanguageError();
    }

    public TTSEngine(TtsEngineListener listener, boolean isSettingsRun) {
        this.listener = listener;
        this.isSettingsRun = isSettingsRun;
    }

    public void enable(Context context) {
        if (isEnabled) {
            listener.onTTSInit();
            return;
        }
        if (isSettingsRun ) {
            Intent checkIntent = new Intent();
            checkIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
            List<ResolveInfo> activities = context.getPackageManager().queryIntentActivities(checkIntent, 0);
            if (activities.size() == 1) {
                listener.invokeApplication(context, activities.get(0).activityInfo.applicationInfo.packageName,
                        activities.get(0).activityInfo.name);
            }
            ArrayList<String> packages = new ArrayList<String>();
            ArrayList<String> packagesActivities = new ArrayList<String>();
            for (ResolveInfo info : activities) {
                packages.add(info.activityInfo.applicationInfo.packageName);
                packagesActivities.add(info.activityInfo.name);
            }
            if (packages.size() == 1 || Build.VERSION.SDK_INT < Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
                listener.invokeApplication(context, packages.get(0), packagesActivities.get(0));
            } else if (packages.size() > 1) {
                listener.startChooserDialog(packages, packagesActivities);
            }
        } else {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
            String packageName = prefs.getString(TTS_PACKAGE, "");
            String packageActivity = prefs.getString(TTS_ACTIVITY, "");
            listener.invokeApplication(context, packageName, packageActivity);
        }

    }

    public boolean onActivityResult(Context context,int requestCode, int resultCode) {
        if (requestCode == MY_DATA_CHECK_CODE) {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
            if (resultCode == TextToSpeech.Engine.CHECK_VOICE_DATA_PASS) {
                prefs.edit().putBoolean(IS_ENABLED, true).apply();
                forceInit(context);
                return true;
            } else {
                prefs.edit().putBoolean(IS_ENABLED, false).apply();
                return false;
            }
        }
        return true;
    }

    public boolean say(Locale locale, String string) {
        if (!isLanguageAvailable(locale) ) {
            Log.d("Test", "no language");
            return false;
        }

        if (tts == null) {
            Log.d("Test", "no tts");
            return false;
        }
        HashMap<String, String> params = new HashMap<String, String>();
        params.put(TextToSpeech.Engine.KEY_PARAM_VOLUME, String.valueOf(volume));
        params.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "stringId");
        tts.speak(string, TextToSpeech.QUEUE_FLUSH, params);
        Log.d("Test", "say time");
        if (listener != null) listener.onTTSPlayStarted();
        return true;
    }

    public void onFinish() {
        if (tts != null) tts.shutdown();
        isEnabled = false;
        localesList.clear();
    }


    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.ERROR && listener != null)
            listener.onTTSError();
        if (tts == null)
            return;

        tts.setLanguage(locale);
        if (listener != null) listener.onTTSInit();
        tts.setOnUtteranceCompletedListener(new TextToSpeech.OnUtteranceCompletedListener() {
            @Override
            public void onUtteranceCompleted(String utteranceId) {
                if (listener != null) listener.onTTSPlayCompleted();
            }
        });
        isEnabled = true;
    }

    public boolean isLanguageAvailable(Locale locale) {
        if (tts == null) return false;

        Locale tmpLocale = new Locale(locale.getLanguage());
        int available = tts.isLanguageAvailable(tmpLocale);
        int code = tts.setLanguage(tmpLocale);

        Log.d("App", locale.getLanguage() + ": LANG_AVAILABLE " + (code == TextToSpeech.LANG_AVAILABLE) + ", " +
                " LANG_COUNTRY_AVAILABLE " + (code == TextToSpeech.LANG_COUNTRY_AVAILABLE) + ", " +
                "LANG_COUNTRY_VAR_AVAILABLE " + (code == TextToSpeech.LANG_COUNTRY_VAR_AVAILABLE) + ", " +
                "LANG_MISSING_DATA " + (code == TextToSpeech.LANG_MISSING_DATA) + ", " +
                "LANG_NOT_SUPPORTED " + (code == TextToSpeech.LANG_NOT_SUPPORTED));

        return code == TextToSpeech.LANG_AVAILABLE && available == TextToSpeech.LANG_AVAILABLE;
    }

    public void getSupportedLanguages(ArrayList<Locale> result) {
        result.clear();
        if (!localesList.isEmpty()) {
            result.addAll(localesList);
            return;
        }

        Locale[] locales = Locale.getAvailableLocales();
        List<String> tmpLocalNames = new ArrayList<String>();
        for (Locale locale : locales)
        {
            if (!tmpLocalNames.contains(locale.getLanguage()) && isLanguageAvailable(locale))
            {
                tmpLocalNames.add(locale.getLanguage());
                localesList.add(locale);
                result.add(locale);
            }
        }
        if (locale != null && tmpLocalNames.contains(locale.getLanguage())){
            tts.setLanguage(locale);
        } else {
            tts.setLanguage(null);
            if (locale != null)
                listener.onTTSLanguageError();
        }
        return;
    }
}
