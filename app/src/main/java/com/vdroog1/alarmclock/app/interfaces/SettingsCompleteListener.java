package com.vdroog1.alarmclock.app.interfaces;

/**
 * Created by kettricken on 02.12.2014.
 */
public interface SettingsCompleteListener {
    public void onSettingsDone(int action, int id);
}
