package com.vdroog1.alarmclock.app.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import com.vdroog1.alarmclock.app.R;

/**
 * Created by KaterinaG on 02.12.2014.
 * Dialog to choose a sound to call
 */
public class AlarmSoundDialog extends SettingsDialog {

    public static final String TAG = AlarmSoundDialog.class.getSimpleName();

    int selectedItem;
    String[] soundNames;

    public AlarmSoundDialog(){}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            selectedItem = savedInstanceState.getInt("selectedItem");
            soundNames = savedInstanceState.getStringArray("soundNames");
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("selectedItem", selectedItem);
        outState.putStringArray("soundNames", soundNames);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle(R.string.set_ringtone_title)
                .setSingleChoiceItems(soundNames, selectedItem,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                selectedItem = which;
                            }
                        })
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        listener.onSoundSelected(selectedItem);
                        dialog.dismiss();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        return builder.create();
    }

    public void setSoundNames(String[] soundNames){
        this.soundNames = soundNames;
    }

    public void setSelectedItem(int selectedItem) {
        this.selectedItem = selectedItem;
    }
}
