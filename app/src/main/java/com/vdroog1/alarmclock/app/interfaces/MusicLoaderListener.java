package com.vdroog1.alarmclock.app.interfaces;

/**
 * Created by kettricken on 04.12.2014.
 */
public interface MusicLoaderListener {
    public void onMusicLoaded(String[] names, Object[] uris, int selectedItem);
}
