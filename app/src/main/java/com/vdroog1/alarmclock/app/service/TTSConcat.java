package com.vdroog1.alarmclock.app.service;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.AudioManager;
import android.media.MediaPlayer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by KaterinaG on 09.11.2014.
 *
 */
public class TTSConcat {

    MediaPlayer mediaPlayer;
    TTSEngine.TtsEngineListener listener;

    int index = 0;
    private float volume = 1f;

    public TTSConcat(TTSEngine.TtsEngineListener listener) {
        this.listener = listener;
    }

    public void onFinish() {
        releaseMP();
    }

    private void releaseMP() {
        if (mediaPlayer != null) {
            try {
                mediaPlayer.release();
                mediaPlayer = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
   }

    public void pronounceTime(Context context, Locale locale,String text, boolean isMale) throws NumberFormatException{
        String[] parts = text.split(":");
        if (parts.length != 2)
            throw new NumberFormatException();
        int hours = Integer.valueOf(parts[0]);
        int minutes = Integer.valueOf(parts[1]);
        pronounceTime(context, locale, hours, minutes, isMale);
    }

    public void pronounceTime(Context context, Locale locale, int hours, int minutes, boolean isMale) throws NumberFormatException{
        List<String> fileNames;
        if (locale.getLanguage().equals("ru")) {
            fileNames = say(hours, minutes, isMale);
        } else {
            fileNames = sayEn(hours, minutes);
        }
        if (listener != null) listener.onTTSPlayStarted();
        say(context, fileNames);
    }

    private void playFile(Context context, List<String> files) {
        try {
            String fileName = "audio/" + files.get(index) + ".mp3";
            AssetFileDescriptor afd = context.getAssets().openFd(fileName);
            mediaPlayer.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.setVolume(volume, volume);
            mediaPlayer.prepare();
            mediaPlayer.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private void say(final Context context, final List<String> files) {
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                index++;
                if (index < files.size()) {
                    mediaPlayer.reset();
                    playFile(context, files);
                } else {
                    if (listener != null) listener.onTTSPlayCompleted();
                    releaseMP();
                    index = 0;
                }
            }
        });
        playFile(context, files);
    }

    public List<String> say(int hours, int minutes, boolean isMale) {

        String gender;

        if (isMale) gender = "z";
        else gender = "g";

        List<String> fileNames = new ArrayList<String>();

        if (hours == 1 || hours == 2) {
            String hoursStr = gender + String.valueOf(hours) + "h";
            fileNames.add(hoursStr);
        } else if (hours > 2 && hours <= 20) {
            String hoursStr = gender + String.valueOf(hours);
            fileNames.add(hoursStr);
        } else {
            String hoursTensStr = gender + "20";
            fileNames.add(hoursTensStr);
            int hourUnits = hours - 20;
            if (hourUnits == 1 || hourUnits == 2) {
                String hoursUnitsStr = gender + String.valueOf(hourUnits) + "h";
                fileNames.add(hoursUnitsStr);
            } else {
                String hoursUnitsStr = gender + String.valueOf(hourUnits);
                fileNames.add(hoursUnitsStr);
            }
        }

        if (hours == 1 || hours == 21) {
            fileNames.add(gender + "hr1");
        } else if ((hours >=2 && hours <= 4) || (hours >= 22 && hours <= 24)) {
            fileNames.add(gender + "hr2");
        } else if ((hours > 4 && hours <=20) || hours == 0) {
            fileNames.add(gender + "hrs");
        }

        if (minutes == 0)
            return fileNames;

        if (minutes == 1 || minutes == 2) {
            String minutesStr = gender + String.valueOf(minutes) + "m";
            fileNames.add(minutesStr);
        }

        int units = 0;
        if (minutes > 2 && minutes <= 20) {
            String minutesStr = gender + String.valueOf(minutes);
            fileNames.add(minutesStr);
        } else {
            int tens = (int) Math.floor(minutes / 10) * 10;
            units = minutes - tens;

            String minutesTensStr = gender + String.valueOf(tens);
            fileNames.add(minutesTensStr);

            if (units > 0) {
                if (units == 1 || units == 2) {
                    String minutesUnitsStr = gender + String.valueOf(units) + "m";
                    fileNames.add(minutesUnitsStr);
                } else {
                    String minutesUnitsStr = gender + String.valueOf(units);
                    fileNames.add(minutesUnitsStr);
                }
            }
        }

        String minutesWordStr;
        if (minutes == 1 || units == 1) {
            minutesWordStr = gender + "min1";
        } else if ((minutes >= 2 && minutes <=4) || (units >= 2 && units <=4)) {
            minutesWordStr = gender + "min2";
        } else {
            minutesWordStr = gender + "mins";
        }
        fileNames.add(minutesWordStr);

        return fileNames;
    }

    private List<String> sayEn(int hours, int minutes) {
        String lan = "en ";

        boolean isAm = true;

        List<String> fileNames = new ArrayList<String>();

        if (hours > 12) {
            isAm = false;
            hours = hours - 12;
        }

        fileNames.add(lan + String.valueOf(hours));

        if (minutes == 0) {
            fileNames.add(lan + "oclock");
            return fileNames;
        }

        if (minutes < 10)
            fileNames.add(lan + "oh");

        if (minutes < 20) {
            fileNames.add(lan + String.valueOf(minutes));
        } else {
            int tens = (int) Math.floor(minutes / 10) * 10;
            int units = minutes - tens;

            String minutesTensStr = lan + String.valueOf(tens);
            fileNames.add(minutesTensStr);

            if (units > 0) {
                fileNames.add(lan + String.valueOf(units));
            }
        }

        if (isAm) {
            fileNames.add(lan + "am");
        } else {
            fileNames.add(lan + "pm");
        }
        return fileNames;
    }

    public void setVolume(float volume) {
        this.volume = volume;
    }

    public float getVolume() {
        return volume;
    }
}
