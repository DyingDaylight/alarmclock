package com.vdroog1.alarmclock.app.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.widget.ArrayAdapter;
import com.vdroog1.alarmclock.app.R;
import com.vdroog1.alarmclock.app.adapter.ChooserArrayAdapter;

import java.util.ArrayList;

/**
 * Created by KaterinaG on 04.12.2014.
 * Dialog to choose a TTS Engine
 */
public class TTSChooserDialog extends SettingsDialog {

    public static final String TAG = TTSChooserDialog.class.getSimpleName();

    ArrayList<String> packages = new ArrayList<String>();
    private ArrayList<String> packageActivities = new ArrayList<String>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            packages = savedInstanceState.getStringArrayList("packages");
            packageActivities = savedInstanceState.getStringArrayList("packageActivities");
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putStringArrayList("packages", packages);
        outState.putStringArrayList("packageActivities", packageActivities);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        ArrayAdapter<String> adapter = new ChooserArrayAdapter(getActivity(), android.R.layout.select_dialog_item,
                android.R.id.text1, packages);

        return  new AlertDialog.Builder(getActivity())
                .setTitle(R.string.app_list_title)
                .setAdapter(adapter, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        listener.onPackageSelected(packages.get(item), packageActivities.get(item));
                    }
                })
                .setCancelable(false)
                .show();
    }

    public void setPackages(ArrayList<String> packages){
        this.packages = packages;
    }

    public void setPackageActivities(ArrayList<String> packageActivities) {
        this.packageActivities = packageActivities;
    }
}
