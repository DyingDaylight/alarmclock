package com.vdroog1.alarmclock.app.interfaces;

/**
 * Created by kettricken on 04.12.2014.
 */
public interface OnAlarmSelected {
    void onAlarmSelected(int id);
}
