package com.vdroog1.alarmclock.app.model;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;
import com.vdroog1.alarmclock.app.DB.DataBaseHelper;
import com.vdroog1.alarmclock.app.DB.HelperFactory;
import com.vdroog1.alarmclock.app.Util;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by kettricken on 03.11.2014.
 */
@DatabaseTable(tableName = "alarms")
public class Alarm implements Parcelable {

    public final static String ENABLED_FIELD = "enabled";
    public final static String TIME_TO_TRIGGER_FIELD = "timeToTrigger";
    public final static String REPEAT_NUM_FIELD = "repeatNum";
    public final static String VOLUME_FIELD = "volume";
    public final static String REPEAT_FIELD = "repeat";
    public final static String TIME_INTERVAL_FILED = "timeInterval";
    public final static String RINGTONE_FILED = "ringtone";
    public final static String VIBRATE_FIELD = "vibrate";
    public final static String NOTE_FIELD = "note";

    @DatabaseField(generatedId = true, columnDefinition = "INTEGER PRIMARY KEY")
    int id = -1;

    @DatabaseField(dataType = DataType.BOOLEAN, columnName = ENABLED_FIELD)
    boolean enabled = false;

    @DatabaseField(dataType = DataType.LONG, columnName = TIME_TO_TRIGGER_FIELD)
    long timeToTrigger = 0;

    @ForeignCollectionField(eager = true)
    private ForeignCollection<Day> days;

    @DatabaseField(dataType = DataType.BOOLEAN, columnName = REPEAT_FIELD)
    boolean repeat = false;

    @DatabaseField(dataType = DataType.LONG, columnName = TIME_INTERVAL_FILED)
    long timeInterval = 5 * 60 * 1000;

    @DatabaseField(dataType = DataType.INTEGER, columnName = REPEAT_NUM_FIELD)
    int repeatNum = 5;

    @DatabaseField(canBeNull = false, dataType = DataType.STRING, columnName = RINGTONE_FILED)
    String ringtone = "";

    @DatabaseField(dataType = DataType.INTEGER, columnName = VOLUME_FIELD)
    int volume = 100;

    @DatabaseField(dataType = DataType.BOOLEAN, columnName = VIBRATE_FIELD)
    boolean vibrate = false;

    @DatabaseField(dataType = DataType.STRING, columnName = NOTE_FIELD)
    String note = "";

    ArrayList<Day> daysList = new ArrayList<Day>();

    public Alarm() {
        fillDays();
    }

    public boolean isRepeat() {
        return repeat;
    }

    public long getTimeToTrigger() {
        return timeToTrigger;
    }

    public long getTimeInterval() {
        return timeInterval;
    }

    public void setEnabled(boolean isChecked) {
        enabled = isChecked;
    }

    public void setRepeat(boolean isChecked) {
        repeat = isChecked;
    }

    public void setVibrate(boolean isChecked) {
        vibrate = isChecked;
    }

    public void setTimeToTrigger(long timeToTrigger) {
        this.timeToTrigger = timeToTrigger;
    }

    public void setTimeInterval(long timeInterval) {
        this.timeInterval = timeInterval;
    }

    public void setRingtone(Uri ringtone) {
        if (ringtone == null){
            this.ringtone = "";
            return;
        }
        this.ringtone = ringtone.toString();
    }

    public void setRingtone(String ringtone) {
        this.ringtone = ringtone;
    }

    public String getRingtone() {
        return ringtone;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public boolean isVibrate() {
        return vibrate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setDaysOfWeek() {
       if (days == null) {
            DataBaseHelper bdHelper = HelperFactory.getHelper();
            bdHelper.createEmptyCollection(this);
        }
        days.clear();
        for (Day day : daysList) {
            day.setAlarm(this);
        }
        days.addAll(daysList);
    }

    public void setDays(ArrayList<Day> days) {
        this.daysList = days;
    }

    public ArrayList<Day> getDays() {
        return daysList;
    }

    @Override
    public int describeContents() {
        return 0;
    }


    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeByte((byte) (enabled ? 1 : 0));
        dest.writeByte((byte) (repeat ? 1 : 0));
        dest.writeByte((byte) (vibrate ? 1 : 0));
        dest.writeLong(timeToTrigger);
        dest.writeLong(timeInterval);
        dest.writeString(ringtone);
        dest.writeTypedList(daysList);
        dest.writeString(note);
        dest.writeInt(repeatNum);
        dest.writeInt(volume);
    }

    private Alarm(Parcel in) {
        id = in.readInt();
        enabled = in.readByte() != 0;
        repeat = in.readByte() != 0;
        vibrate = in.readByte() != 0;
        timeToTrigger = in.readLong();
        timeInterval = in.readLong();
        ringtone = in.readString();
        daysList.clear();
        daysList = in.createTypedArrayList(Day.CREATOR);
        note = in.readString();
        repeatNum = in.readInt();
        volume = in.readInt();
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Alarm createFromParcel(Parcel in) {
            return new Alarm(in);
        }

        public Alarm[] newArray(int size) {
            return new Alarm[size];
        }
    };

    public Collection<Day> getDaysOfWeek() {
        return days;
    }

    public void fillDays() {
        if (days != null && daysList.isEmpty()) {
            daysList.addAll(days);
        }
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public int getRepeatNum() {
        return repeatNum;
    }

    public void setRepeatNum(int repeatNum) {
        this.repeatNum = repeatNum;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }

    public int getVolume() {
        return volume;
    }

    @Override
    public String toString() {
        String string = "id = " + id + "; " +
                "time = " + Util.formatTime(timeToTrigger) + "; " +
                "repeat = " + repeat + " " +  repeatNum + " times " + " every " +
                Util.timeIntervalInMinutes(timeInterval) + " minutes; ";
        return string;
    }
}
