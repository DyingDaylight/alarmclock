package com.vdroog1.alarmclock.app.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.*;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;
import com.vdroog1.alarmclock.app.DB.DataBaseHelper;
import com.vdroog1.alarmclock.app.DB.HelperFactory;
import com.vdroog1.alarmclock.app.R;
import com.vdroog1.alarmclock.app.Util;
import com.vdroog1.alarmclock.app.adapter.AlarmListAdapter;
import com.vdroog1.alarmclock.app.interfaces.AlarmServiceListener;
import com.vdroog1.alarmclock.app.interfaces.OnAlarmEnabledListener;
import com.vdroog1.alarmclock.app.interfaces.OnAlarmSelected;
import com.vdroog1.alarmclock.app.model.Alarm;
import com.vdroog1.alarmclock.app.service.AlarmService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by KaterinaG on 29.11.2014.
 * Fragment to show alarm list
 */
public class AlarmsListFragment extends Fragment implements OnAlarmEnabledListener, AlarmServiceListener {

    public static final String TAG = AlarmsListFragment.class.getSimpleName();

    List<Alarm> alarms = new ArrayList<Alarm>();

    View mView;
    View bNewAlarm;
    ListView alarmsLV;
    AlarmListAdapter alarmListAdapter;

    DataBaseHelper dbHelper;

    OnAlarmSelected listener;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRetainInstance(true);

        dbHelper = HelperFactory.getHelper();
        alarms = dbHelper.getAlarmsList();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.activity_main, container, false);

        bNewAlarm = mView.findViewById(R.id.b_new_alarm);
        alarmsLV = (ListView) mView.findViewById(R.id.lv_alarms);

        alarmListAdapter = new AlarmListAdapter(getActivity(), R.layout.list_item, alarms, this);
        alarmsLV.setAdapter(alarmListAdapter);

        alarmsLV.setOnItemClickListener(getOnAlarmClickListener());

        registerForContextMenu(alarmsLV);

        bNewAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onAlarmSelected(-1);
            }
        });

        return mView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof OnAlarmSelected) {
            listener = (OnAlarmSelected) activity;
        } else {
            throw new ClassCastException(activity.toString()
                    + " must implemenet MyListFragment.OnItemSelectedListener");
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        if (v.getId() == R.id.lv_alarms) {
            getActivity().getMenuInflater().inflate(R.menu.context_menu, menu);

            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
            int index = info.position;
            Alarm alarm = alarms.get(index);

            MenuItem menuItem = menu.findItem(R.id.menu_enable);
            if (alarm.isEnabled())
                menuItem.setTitle(getString(R.string.c_disable));
            else
                menuItem.setTitle(getString(R.string.c_enable));
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        int index = info.position;
        Alarm alarm = alarms.get(index);
        switch (item.getItemId()) {
            case R.id.menu_enable:
                enableAlarm(index, !alarm.isEnabled());
                break;
            case R.id.menu_edit:
                listener.onAlarmSelected(alarm.getId());
                break;
            case R.id.menu_delete:
                deleteAlarm(alarm);
                break;
        }

        return true;
    }

    @Override
    public void onCheckChanged(int index, boolean isChecked) {
        enableAlarm(index, isChecked);
    }

    @Override
    public void onAlarmEnabled(boolean isEnabled, int id) {
        for (Alarm alarm : alarms) {
            if (alarm.getId() == id) {
                alarm.setEnabled(isEnabled);
                break;
            }
        }
        alarmListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onAlarmSaved(int id) {
        int index = -1;
        for (Alarm alarm : alarms) {
            if (alarm.getId() == id) {
                index = alarms.indexOf(alarm);
                alarms.remove(alarm);
                break;
            }
        }
        Alarm alarm = dbHelper.getAlarm(id);
        if (index != -1)
            alarms.add(index, alarm);
        else
            alarms.add(alarm);
        alarmListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onAlarmDeleted(int id) {
        for (Alarm alarm : alarms) {
            if (alarm.getId() == id) {
                alarms.remove(alarm);
                break;
            }
        }
        alarmListAdapter.notifyDataSetChanged();
    }

    private AdapterView.OnItemClickListener getOnAlarmClickListener() {
        return new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Alarm alarm = alarms.get(position);
                listener.onAlarmSelected(alarm.getId());
            }
        };
    }

    private void enableAlarm(int index, boolean isChecked) {
        Alarm alarm = alarms.get(index);
        Intent mServiceIntent = new Intent(getActivity(), AlarmService.class);
        mServiceIntent.setAction(AlarmService.ACTION_CHANGE_STATE);
        mServiceIntent.putExtra(AlarmService.ALARM_ID, alarm.getId());
        mServiceIntent.putExtra(AlarmService.ENABLED, isChecked);
        getActivity().startService(mServiceIntent);
        if (isChecked) {
            String timeBeforeTrigger = Util.showTimeToTrigger(getActivity(), alarm.getTimeToTrigger(), alarm.getDays());
            Toast.makeText(getActivity(), timeBeforeTrigger, Toast.LENGTH_LONG).show();
        }
    }

    private void deleteAlarm(Alarm alarm) {
        Intent mServiceIntent = new Intent(getActivity(), AlarmService.class);
        mServiceIntent.setAction(AlarmService.ACTION_DELETE);
        mServiceIntent.putExtra(AlarmService.ALARM_ID, alarm.getId());
        getActivity().startService(mServiceIntent);
    }
}
