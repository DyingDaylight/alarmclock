package com.vdroog1.alarmclock.app.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import com.vdroog1.alarmclock.app.R;
import com.vdroog1.alarmclock.app.Util;
import com.vdroog1.alarmclock.app.fragment.SettingsFragment;
import com.vdroog1.alarmclock.app.interfaces.SettingsCompleteListener;
import com.vdroog1.alarmclock.app.service.AlarmService;

/**
 * Created by KaterinaG on 03.11.2014.
 * Settings for a particular alarm.
 */
public class SettingsActivity extends ActionBarActivity implements SettingsCompleteListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.fragment_activity);

        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = fragmentManager.findFragmentByTag(SettingsFragment.TAG);
        if (fragment == null) {
            Bundle args = new Bundle();
            int id = getIntent().getIntExtra(Util.ALARM_ID, -1);
            args.putInt(Util.ALARM_ID, id);

            fragment = new SettingsFragment();
            fragment.setArguments(args);
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.add(R.id.fragment_container, fragment, SettingsFragment.TAG);
            fragmentTransaction.commit();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter(AlarmService.UPDATE);
        registerReceiver(dataBaseEventsReceiver, filter);
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(dataBaseEventsReceiver);
    }

    @Override
    public void onSettingsDone(int action, int id) {
        Intent intent = new Intent();
        intent.putExtra(Util.ACTION, action);
        intent.putExtra(Util.ALARM_ID, id);
        setResult(RESULT_OK, intent);
        finish();
    }

    private BroadcastReceiver dataBaseEventsReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle extras = intent.getExtras();
            if (extras != null) {
                int action = extras.getInt(Util.ACTION);
                int id = extras.getInt(Util.ALARM_ID, -1);
                onSettingsDone(action, id);
            }
        }
    };
}
