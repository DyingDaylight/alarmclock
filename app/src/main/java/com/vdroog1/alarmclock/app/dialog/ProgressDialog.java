package com.vdroog1.alarmclock.app.dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.Loader;
import com.vdroog1.alarmclock.app.R;

/**
 * Created by KaterinaG on 05.12.2014.
 * Progress dialog to show while music is loading
 */
public class ProgressDialog extends SettingsDialog{

    public static final String TAG = ProgressDialog.class.getSimpleName();
    private int type;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null)
            type = savedInstanceState.getInt("type");
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("type", type);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        android.app.ProgressDialog progressDialog = new android.app.ProgressDialog(getActivity());
        progressDialog.setMessage(getString(R.string.loading_music));
        progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                Loader<Cursor> loader = getActivity().getSupportLoaderManager().getLoader(type);
                if (loader != null) loader.stopLoading();
            }
        });
        return progressDialog;
    }

    public void setType(int type) {
        this.type = type;
    }
}
