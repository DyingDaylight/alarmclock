package com.vdroog1.alarmclock.app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import com.vdroog1.alarmclock.app.R;
import com.vdroog1.alarmclock.app.Util;
import com.vdroog1.alarmclock.app.interfaces.OnAlarmEnabledListener;
import com.vdroog1.alarmclock.app.model.Alarm;
import com.vdroog1.alarmclock.app.view.AlarmItemView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by KaterinaG on 03.11.2014.
 * Adapter tpo populate list of alarmClocks
 */
public class AlarmListAdapter extends ArrayAdapter<String> {

    OnAlarmEnabledListener listener;

    Context context;

    List<Alarm> items = new ArrayList<Alarm>();

    public AlarmListAdapter(Context context, int resource, List<Alarm> objects, OnAlarmEnabledListener listener) {
        super(context, resource);
        items = objects;
        this.context = context;
        this.listener = listener;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.list_item, parent, false);
        } else {
            view = convertView;
        }

        if (view instanceof AlarmItemView) {
            Alarm alarm = items.get(position);

            AlarmItemView alarmView = (AlarmItemView) view;

            String time = Util.formatTime(alarm.getTimeToTrigger());
            alarmView.setTime(time);

            alarm.fillDays();
            String daysOfWeek = Util.getDaysOfWeek(context, alarm.getDays());
            alarmView.setDaysOfWeek(daysOfWeek);

            String note = alarm.getNote();
            alarmView.setNote(note);

            alarmView.setChecked(items.get(position).isEnabled());
            alarmView.setListener(listener, position);
        }
        return view;
    }

    @Override
    public int getCount() {
        return items.size();
    }
}
