package com.vdroog1.alarmclock.app.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.NumberPicker;
import com.vdroog1.alarmclock.app.R;

/**
 * Created by KaterinaG on 02.12.2014.
 * Dialog to choose repeat number
 */
public class RepeatDialog extends SettingsDialog {

    public static final String TAG = RepeatDialog.class.getSimpleName();

    private NumberPicker numberPicker;

    private int repeatNum;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            repeatNum = savedInstanceState.getInt("repeatNum");
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        repeatNum = numberPicker.getValue();
        outState.putInt("repeatNum", repeatNum);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view  = layoutInflater.inflate(R.layout.number_picker, null, false);
        numberPicker = (NumberPicker) view.findViewById(R.id.numberPicker1);
        numberPicker.setMaxValue(10);
        numberPicker.setMinValue(2);
        numberPicker.setWrapSelectorWheel(false);
        numberPicker.setValue(repeatNum);
        numberPicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);

        return new AlertDialog.Builder(getActivity())
                .setTitle(getString(R.string.set_repeat_num))
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        listener.onRepeatNumSelected(numberPicker.getValue());
                        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                    }
                })
                .setNegativeButton(getString(R.string.cancel),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                            }
                        })
                .setView(view)
                .show();
    }

    public void setRepeatNum(int repeatNum) {
        this.repeatNum = repeatNum;
    }
}
