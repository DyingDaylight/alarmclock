package com.vdroog1.alarmclock.app.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.*;
import com.vdroog1.alarmclock.app.R;
import com.vdroog1.alarmclock.app.interfaces.OnAlarmEnabledListener;

/**
 * Created by kettricken on 03.11.2014.
 */
public class AlarmItemView extends LinearLayout implements Checkable {

    private CheckBox checkbox;
    private TextView title;
    private TextView subtitle;
    private TextView note;

    public AlarmItemView(Context context) {
        super(context);
        inflate(context, R.layout.alarm_item, this);
        this.checkbox = (CheckBox) findViewById(R.id.checkbox);
        this.title = (TextView) findViewById(R.id.tv_time);
        this.subtitle = (TextView) findViewById(R.id.tv_days);
        this.note = (TextView) findViewById(R.id.tv_note);
    }

    public AlarmItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflate(context, R.layout.alarm_item, this);
        this.checkbox = (CheckBox) findViewById(R.id.checkbox);
        this.title = (TextView) findViewById(R.id.tv_time);
        this.subtitle = (TextView) findViewById(R.id.tv_days);
        this.note = (TextView) findViewById(R.id.tv_note);
    }

    @Override
    public void setChecked(boolean checked) {
        if (checkbox != null) {
            checkbox.setChecked(checked);
        }
    }

    @Override
    public boolean isChecked() {
        return checkbox != null ? checkbox.isChecked() : false;
    }

    @Override
    public void toggle() {
        if (checkbox != null) {
            checkbox.toggle();
        }
    }

    public void setTime(String title){
        if (title != null) this.title.setText(title);
    }

    public void setDaysOfWeek(String subtitle) {
        if (subtitle != null) this.subtitle.setText(subtitle);
    }

    public void setNote(String noteStr) {
        if (note != null) this.note.setText(noteStr);
    }

    public void setListener(final OnAlarmEnabledListener listener, int id) {
        checkbox.setTag(id);
        checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                listener.onCheckChanged((Integer) buttonView.getTag(), isChecked);
            }
        });
    }
}
