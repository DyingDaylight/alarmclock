package com.vdroog1.alarmclock.app.interfaces;

/**
 * Created by kettricken on 04.12.2014.
 */
public interface OnAlarmEnabledListener {
    void onCheckChanged(int index, boolean isChecked);
}
