package com.vdroog1.alarmclock.app.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.vdroog1.alarmclock.app.R;
import com.vdroog1.alarmclock.app.Util;
import com.vdroog1.alarmclock.app.dialog.ErrorLanguageDialog;
import com.vdroog1.alarmclock.app.dialog.ErrorTTSDialog;
import com.vdroog1.alarmclock.app.dialog.TTSChooserDialog;
import com.vdroog1.alarmclock.app.interfaces.SettingsDialogsListener;
import com.vdroog1.alarmclock.app.service.TTSConcat;
import com.vdroog1.alarmclock.app.service.TTSEngine;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by KaterinaG on 29.11.2014.
 * Fragment for TTS settings
 */
public class TTSSettingsFragment extends TTSFragment implements SettingsDialogsListener {

    public static final String TAG = TTSSettingsFragment.class.getSimpleName();

    View mView;
    EditText et;
    View tvWarning;
    View genderSettings;
    CheckBox cbEnableTts;
    RadioGroup rg;

    TTSEngine ttsEngine;
    TTSConcat ttsConcat;

    Context context;
    SharedPreferences prefs;
    Locale locale;

    ArrayList<Locale> ttsLocales = new ArrayList<Locale>();

    ProgressDialog progressDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRetainInstance(true);

        ttsEngine = new TTSEngine(this, true);
        ttsConcat = new TTSConcat(this);

        prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());

        String localeStr = prefs.getString(Util.LOCALE, "en");
        locale = new Locale(localeStr);

        boolean isTTSEnabled = prefs.getBoolean(Util.TTS_ENABLED, false);

        if (isTTSEnabled) {
            ttsEngine.enable(getActivity());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        context = getActivity();

        mView = inflater.inflate(R.layout.tts_settings, container, false);

        et = (EditText) mView.findViewById(R.id.et);
        cbEnableTts = (CheckBox) mView.findViewById(R.id.enableTts);
        rg = (RadioGroup) mView.findViewById(R.id.rg);
        tvWarning = mView.findViewById(R.id.tv_internet_warning);
        genderSettings =  mView.findViewById(R.id.gender_settings);

        cbEnableTts.setChecked(prefs.getBoolean(Util.TTS_ENABLED, false));

        rg.setOnCheckedChangeListener(onLanguageChanged);

        cbEnableTts.setOnCheckedChangeListener(onTTSChanged);

        mView.findViewById(R.id.b).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String text = et.getText().toString();
                    Log.d("App", "locale  " + locale.getLanguage());
                    if (cbEnableTts.isChecked()) {
                        boolean isLanguageSet = ttsEngine.say(locale, text);
                        if (!isLanguageSet)
                            showErrorLanguageDialog();
                    } else {
                        boolean isMale = prefs.getBoolean(Util.IS_MALE, false);
                        ttsConcat.pronounceTime(context, locale, text, isMale);
                    }
                } catch (NumberFormatException e) {
                    Toast.makeText(getActivity(), getString(R.string.time_format_error), Toast.LENGTH_SHORT).show();
                }
            }
        });
        return mView;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (cbEnableTts.isChecked()) {
            tvWarning.setVisibility(View.VISIBLE);
        } else {
            tvWarning.setVisibility(View.GONE);
        }
        setLanguageButtons();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ttsEngine.onFinish();
        ttsConcat.onFinish();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        boolean isTTSEngineAvailable = ttsEngine.onActivityResult(context.getApplicationContext(), requestCode, resultCode);
        if(!isTTSEngineAvailable)
            showNoTTSDialog();
    }

    @Override
    public void onTTSInit() {
        Log.d("app", "onTTSInit");
        new TTSEngineEnable().execute();
    }

    @Override
    public void onTTSError() {
        Toast.makeText(context, getString(R.string.error_tts_init), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onTTSPlayCompleted() { }

    @Override
    public void onTTSPlayStarted() { }

    @Override
    public void invokeApplication(Context context, String packageName, String className) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(TTSEngine.TTS_PACKAGE, packageName);
        editor.putString(TTSEngine.TTS_ACTIVITY, className);
        editor.apply();
        super.invokeApplication(context, packageName, className);
    }

    @Override
    public void startChooserDialog(ArrayList<String> packages, ArrayList<String> packageActivities) {
        TTSChooserDialog chooserDialog = new TTSChooserDialog();
        chooserDialog.setPackages(packages);
        chooserDialog.setPackageActivities(packageActivities);
        chooserDialog.show(getActivity().getSupportFragmentManager(), TTSChooserDialog.TAG);
    }

    @Override
    public void onTTSLanguageError() {
        Toast.makeText(context, getString(R.string.error_no_language), Toast.LENGTH_SHORT).show();
        rg.clearCheck();
    }

    private void showErrorLanguageDialog() {
        ErrorLanguageDialog errorLanguageDialog = new ErrorLanguageDialog();
        errorLanguageDialog.show(getActivity().getSupportFragmentManager(), ErrorLanguageDialog.TAG);
    }

    private void showNoTTSDialog() {
        ErrorTTSDialog errorTTSDialog = new ErrorTTSDialog();
        errorTTSDialog.show(getActivity().getSupportFragmentManager(), ErrorTTSDialog.TAG);
    }

    private void setLanguageButtons() {
        if (cbEnableTts.isChecked())
            setTTSLanguages();
        else
            setNativeLanguages();
    }

    private void setTTSLanguages() {
        for (Locale locale : ttsLocales)
        {
            RadioButton rbEn = new RadioButton(getActivity());
            rbEn.setText(locale.getDisplayLanguage());
            rbEn.setTag(locale.getLanguage());
            rg.addView(rbEn);
            if (locale.getLanguage().equals(this.locale.getLanguage())) {
                rbEn.setChecked(true);
            }
        }
    }

    private void setNativeLanguages() {
        RadioButton rbEn = new RadioButton(getActivity());
        rbEn.setText(getString(R.string.english));
        rbEn.setTag("en");
        rg.addView(rbEn);

        RadioButton rbRus = new RadioButton(getActivity());
        rbRus.setText(getString(R.string.russian));
        rbRus.setTag("ru");
        rg.addView(rbRus);

        rbRus.setOnCheckedChangeListener(onGenderContainingLanguageChanged);

        if (locale.getLanguage().equals(rbEn.getTag())) {
            rbEn.setChecked(true);
        } else if (locale.getLanguage().equals(rbRus.getTag())) {
            rbRus.setChecked(true);
            genderSettings.setVisibility(View.VISIBLE);
            if (prefs.getBoolean(Util.IS_MALE, false)) {
                ((RadioButton) mView.findViewById(R.id.rb_male)).setChecked(true);
            } else {
                ((RadioButton) mView.findViewById(R.id.rb_female)).setChecked(true);
            }
        } else {
            rbEn.setChecked(true);
            prefs.edit().putString(Util.LOCALE, (String) rbEn.getTag()).apply();
        }

        ((RadioGroup) mView.findViewById(R.id.rg_gender)).setOnCheckedChangeListener(onGenderCheckChanged);
    }

    private RadioGroup.OnCheckedChangeListener onGenderCheckChanged = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            switch (checkedId) {
                case R.id.rb_female:
                    prefs.edit().putBoolean(Util.IS_MALE, false).apply();
                    break;
                case R.id.rb_male:
                    prefs.edit().putBoolean(Util.IS_MALE, true).apply();
                    break;
                default:
                    break;
            }
        }
    };

    private CompoundButton.OnCheckedChangeListener onGenderContainingLanguageChanged = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (isChecked) {
                mView.findViewById(R.id.gender_settings).setVisibility(View.VISIBLE);
                if (prefs.getBoolean(Util.IS_MALE, false)) {
                    ((RadioButton) mView.findViewById(R.id.rb_male)).setChecked(true);
                } else {
                    ((RadioButton) mView.findViewById(R.id.rb_female)).setChecked(true);
                }
            } else {
                mView.findViewById(R.id.gender_settings).setVisibility(View.GONE);
            }
        }
    };

    private RadioGroup.OnCheckedChangeListener onLanguageChanged = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            RadioButton rb = (RadioButton) group.findViewById(checkedId);
            if (rb == null || rb.getTag() == null) return;
            locale = new Locale((String) rb.getTag());
            prefs.edit().putString(Util.LOCALE, locale.getLanguage()).apply();
        }
    };

    private CompoundButton.OnCheckedChangeListener onTTSChanged = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            rg.removeAllViews();
            genderSettings.setVisibility(View.GONE);
            if (isChecked) {
                prefs.edit().putBoolean(Util.TTS_ENABLED, true).apply();
                ttsConcat.onFinish();
                Log.d("App", "onCheckChanged enable");
                ttsEngine.enable(context);
                tvWarning.setVisibility(View.VISIBLE);
            } else {
                prefs.edit().putBoolean(Util.TTS_ENABLED, false).apply();
                ttsLocales.clear();
                ttsEngine.onFinish();
                setLanguageButtons();
                tvWarning.setVisibility(View.GONE);
            }
        }
    };

    @Override
    public void onRingtoneOptionSelected() {}

    @Override
    public void onMusicOptionSelected() {}

    @Override
    public void onSoundSelected(int position) {}

    @Override
    public void onDaysSelected(boolean[] daysToChoose) {}

    @Override
    public void onIntervalSelected(long intervalInMillis) {}

    @Override
    public void onRepeatNumSelected(int repeatNum) {}

    @Override
    public void onTimeSelected(long timeInMillis) {}

    @Override
    public void onPackageSelected(String packageName, String activityName) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(getString(R.string.progress));
        progressDialog.setCancelable(false);
        progressDialog.show();
        invokeApplication(getActivity(), packageName, activityName);
    }

    private class TTSEngineEnable extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (progressDialog == null) {
                progressDialog = new ProgressDialog(context);
                progressDialog.setMessage(getString(R.string.progress));
                progressDialog.show();
            }
        }

        @Override
        protected Void doInBackground(Void... params) {
            if (ttsLocales.isEmpty()) {
            ttsEngine.getSupportedLanguages(ttsLocales);
        }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressDialog.dismiss();
            setLanguageButtons();
        }
    }
}
