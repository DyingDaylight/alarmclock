package com.vdroog1.alarmclock.app.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.vdroog1.alarmclock.app.DB.DataBaseHelper;
import com.vdroog1.alarmclock.app.DB.HelperFactory;
import com.vdroog1.alarmclock.app.activity.StopActivity;

import java.util.Calendar;

/**
 * Created by kettricken on 03.11.2014.
 */
public class AlarmReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        int alarmId = intent.getIntExtra(AlarmService.ALARM_ID, -1);
        int dayId = intent.getIntExtra(AlarmService.DAY_ID, -1);

        Calendar calendar = Calendar.getInstance();
        AlarmService.addLog("========================================\n" +
                "AlarmReceiver today is " + calendar.get(Calendar.DAY_OF_MONTH) +
                "." + (calendar.get(Calendar.MONTH) + 1) +
                "." + calendar.get(Calendar.YEAR) +
                " " + calendar.get(Calendar.HOUR_OF_DAY)
                + ":" + calendar.get(Calendar.MINUTE));
        AlarmService.addLog("AlarmReceiver:onReceive " + alarmId);

        HelperFactory.setHelper(context);
        DataBaseHelper dbHelper = HelperFactory.getHelper();
        if (!dbHelper.ifDayExists(dayId)) {
            AlarmService.addLog("AlarmReceiver:onReceive alarm " + alarmId + " doesn't exist");
            Intent mServiceIntent = new Intent(context, AlarmService.class);
            mServiceIntent.setAction(AlarmService.ACTION_STOP);
            mServiceIntent.putExtra(AlarmService.DAY_ID, dayId);
            context.startService(mServiceIntent);
            return;
        }

        Intent stopIntent = new Intent(context, StopActivity.class);
        stopIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        stopIntent.putExtras(intent.getExtras());
        context.startActivity(stopIntent);
    }
}
