package com.vdroog1.alarmclock.app.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.WindowManager;
import com.vdroog1.alarmclock.app.R;
import com.vdroog1.alarmclock.app.fragment.StopFragment;
import com.vdroog1.alarmclock.app.service.AlarmService;

/**
 * Created by KaterinaG on 07.11.2014.
 * Container for stop fragment
 */
public class StopActivity extends FragmentActivity {

    StopFragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.d("App", "StopActivity onCreate");

        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN |
                        WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
                        WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                        WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON,
                        WindowManager.LayoutParams.FLAG_FULLSCREEN |
                        WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
                        WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                        WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON |
                        WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
        );

        setContentView(R.layout.fragment_activity);

        Intent intent = getIntent();
        int alarmId = intent.getIntExtra(AlarmService.ALARM_ID, -1);
        int dayId = intent.getIntExtra(AlarmService.DAY_ID, -1);

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragment = (StopFragment) fragmentManager.findFragmentByTag(StopFragment.TAG);
        if (fragment == null) {
            Bundle bundle = new Bundle();
            bundle.putInt(AlarmService.ALARM_ID, alarmId);
            bundle.putInt(AlarmService.DAY_ID, dayId);

            fragment = new StopFragment();
            fragment.setArguments(bundle);
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.add(R.id.fragment_container, fragment, StopFragment.TAG);
            fragmentTransaction.commit();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        fragment.onBackPressed();
    }
}
