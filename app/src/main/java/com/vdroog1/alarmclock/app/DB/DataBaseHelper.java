package com.vdroog1.alarmclock.app.DB;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.vdroog1.alarmclock.app.model.Alarm;
import com.vdroog1.alarmclock.app.model.Day;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by KaterinaG on 04.11.2014.
 * Helper to work with DB
 */
public class DataBaseHelper extends OrmLiteSqliteOpenHelper{

    private static final String TAG = DataBaseHelper.class.getSimpleName();

    private static final String DATABASE_NAME ="alarms.db";

    private static final int DATABASE_VERSION = 1;

    public DataBaseHelper(Context context){
        super(context,DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource){
        try
        {
            TableUtils.createTable(connectionSource, Alarm.class);
            TableUtils.createTable(connectionSource, Day.class);
        }
        catch (SQLException e){
            Log.e(TAG, "error creating DB " + DATABASE_NAME);
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVer, int newVer){
        try{
            TableUtils.dropTable(connectionSource, Alarm.class, true);
            TableUtils.dropTable(connectionSource, Day.class, true);
            onCreate(db, connectionSource);
        }
        catch (SQLException e){
            Log.e(TAG, "error upgrading db " + DATABASE_NAME + "from ver " + oldVer);
            throw new RuntimeException(e);
        }
    }

    public void saveAlarm(Alarm alarm) {
        try {
            Dao<Alarm, Integer> dao = getDao(Alarm.class);
            Dao.CreateOrUpdateStatus status = dao.createOrUpdate(alarm);
            if (status.isCreated()) {
                int id = dao.extractId(alarm);
                alarm.setId(id);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteAlarm(Alarm alarm) {
        try {
            Dao<Alarm, Integer> alarmIntegerDao = getDao(Alarm.class);
            Dao<Day, Integer> daysDao = getDao(Day.class);
            Collection<Day> daysOfWeek = alarm.getDaysOfWeek();
            if (daysOfWeek != null) daysDao.delete(daysOfWeek);
            alarmIntegerDao.delete(alarm);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Alarm getAlarm(int id) {
        try {
            Dao<Alarm, Integer> dao = getDao(Alarm.class);
            return dao.queryForId(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Alarm> getAlarmsList() {
        List<Alarm> alarms = new ArrayList<Alarm>();
        try {
            Dao<Alarm, Integer> dao = getDao(Alarm.class);
            alarms = dao.queryForAll();
            return alarms;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return alarms;
    }

    public boolean ifDayExists(int id) {
        try {
            Dao<Day, Integer> dao = getDao(Day.class);
            return dao.idExists(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public void createEmptyCollection(Alarm alarm) {
        try {
            Dao<Alarm, Integer> dao = getDao(Alarm.class);
            dao.assignEmptyForeignCollection(alarm, "days");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}

