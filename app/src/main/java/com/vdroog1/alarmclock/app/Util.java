package com.vdroog1.alarmclock.app;

import android.content.Context;
import android.content.CursorLoader;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import com.vdroog1.alarmclock.app.model.Alarm;
import com.vdroog1.alarmclock.app.model.Day;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by kettricken on 06.11.2014.
 */
public class Util {

    public static final String TTS_ENABLED = "TTS_enabled";
    public static final String LOCALE = "locale";
    public static final String IS_MALE = "is_male";
    public static final String ALARM_ID = "alarmId";
    public static final String ENABLED = "enabled";
    public static final String ACTION = "my_action";

    public static final int ACTION_CANCEL = 101;
    public static final int ACTION_DELETE = 102;
    public static final int ACTION_SAVE = 103;
    public static final int ACTION_UPDATE = 104;

    public static String formatTime(long timeInMillis) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timeInMillis);

        String hours = String.valueOf(calendar.get(Calendar.HOUR_OF_DAY));
        hours = (hours.length() == 1 ? "0" + hours : hours);

        String minutes = String.valueOf(calendar.get(Calendar.MINUTE));
        minutes = (minutes.length() == 1 ? "0" + minutes : minutes);

        String timeStr = hours + ":" + minutes;
        return timeStr;
    }

    public static String getDaysOfWeek(Context context, List<Day> days) {
        if (days.isEmpty())
            return context.getString(R.string.never);

        Collections.sort(days);
        String daysStr = "";
        String[] daysAr = context.getResources().getStringArray(R.array.days_of_week_short);
        for (int i = 0; i < days.size(); i++) {
            daysStr += daysAr[days.get(i).getIndex()];
            if (i != days.size() - 1) {
                daysStr += ", ";
            }
        }
        return daysStr;
    }

    public static String formatTimeInterval(Context context, long timeIntervalInMillis){
        if (timeIntervalInMillis == 0)
            return context.getString(R.string.none);
        int timeInMinutes = (int) (timeIntervalInMillis / (1000 * 60));
        Resources res = context.getResources();
        String minutes = timeInMinutes + " " + res.getQuantityString(R.plurals.minutes, timeInMinutes);
        return minutes;
    }

    public static int timeIntervalInMinutes(long timeInterval) {
        return (int) (timeInterval / (1000 * 60));
    }

    public static void findDays(Alarm alarm, ArrayList<Day> days, boolean[] daysToChoose){
        days.clear();
        for (int i = 0; i < daysToChoose.length; i++) {
            if (daysToChoose[i]) {
                Day day = new Day(alarm, i + 2);
                if (day.getDayOfWeek() == 8) day.setDayOfWeek(1);
                day.setIndex(i);
                days.add(day);
            }
        }
    }

    public static String formatRepeatTimes(Context context, int repeatNum) {
        if (repeatNum == 0)
            return context.getString(R.string.none);
        Resources res = context.getResources();
        String minutes = repeatNum + " " + res.getQuantityString(R.plurals.times, repeatNum);
        return minutes;
    }

    public static String showTimeToTrigger(Context context, long timeToTrigger, ArrayList<Day> daysList) {
        Calendar now = Calendar.getInstance();
        Calendar trigger = Calendar.getInstance();

        Day closestDay = null;
        long diff = Long.MAX_VALUE;

        for (Day day : daysList) {
            trigger.setTimeInMillis(timeToTrigger);
            trigger.set(Calendar.DAY_OF_WEEK, day.getDayOfWeek());

            ifTriggerBeforeToday(now, trigger, day);


            long tmpDiff = trigger.getTimeInMillis() - now.getTimeInMillis();
            if (tmpDiff > 0 && tmpDiff < diff) {
                diff = tmpDiff;
                closestDay = day;
            }
        }

        trigger.setTimeInMillis(timeToTrigger);
        trigger.set(Calendar.DAY_OF_WEEK, closestDay.getDayOfWeek());

        long diffInSec = TimeUnit.MILLISECONDS.toSeconds(diff);
        long seconds = diffInSec % 60;
        diffInSec /= 60;
        long minutes = diffInSec % 60;
        diffInSec /= 60;
        long hours = diffInSec % 24;
        diffInSec /= 24;
        long days = diffInSec;

        String timeBefore = context.getString(R.string.alarm_fire_in) + " ";
        String time = "";
        Resources res = context.getResources();

        if (days != 0) {
            String daysStr = String.valueOf(days);
            time += daysStr + " " + res.getQuantityString(R.plurals.days, (int) days) + " ";
        }

        if (hours != 0) {
            String hoursStr = String.valueOf(hours);
            time += hoursStr + " " + res.getQuantityString(R.plurals.hours, (int) hours) + " ";
        }

        if (minutes != 0) {
            String minutesStr = String.valueOf(minutes);
            time += minutesStr + " " + res.getQuantityString(R.plurals.minutes, (int) minutes);
        }

        if (time.isEmpty()) {
            return context.getString(R.string.fire_now);
        }

        timeBefore += " " + time;

        return timeBefore;
    }

    public static void ifTriggerBeforeToday(Calendar now, Calendar trigger, Day day) {
        if(trigger.before(now)){
            trigger.set(Calendar.DATE, now.get(Calendar.DATE));
            trigger.set(Calendar.MONTH, now.get(Calendar.MONTH));
            trigger.set(Calendar.YEAR, now.get(Calendar.YEAR));
            trigger.add(Calendar.DATE, 7);
            trigger.set(Calendar.DAY_OF_WEEK, day.getDayOfWeek());
        }
    }

    public static String getRealPathFromURI(Context mContext, Uri contentUri) {
        String[] proj = { MediaStore.Images.Media.DATA };
        CursorLoader loader = new CursorLoader(mContext, contentUri, proj, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }
}
